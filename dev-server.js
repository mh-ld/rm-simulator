const express = require('express');
const path = require('path');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/dev/cec/simulator', express.static(path.join(__dirname, '/dev/cec/simulator')));
app.use('/dev/cec/simulator/*', function (req, res, next) {
  res.sendFile(path.join(__dirname, '/dev/cec/simulator', 'index.html'));
});

app.listen(8080);