export const application = 'rm-simulator';

export const apiURL = 'https://www.marriott-ld.com/api/cec-simulator';

export const rootURL = '/rm-portal';

export const socketURL = 'https://www.marriott-ld.com';