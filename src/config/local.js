export const application = 'rm-simulator';

export const apiURL = 'http://localhost:4000/api/cec-simulator';

export const rootURL = '/rm-portal';

export const socketURL = 'http://localhost:4000';