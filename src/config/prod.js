export const application = 'rm-simulator';

export const apiURL = 'https://www.marriott-ld.com/api/cec-simulator';

export const gaId = 'UA-91016801-4';

export const rootURL = '/rm-portal';

export const socketURL = 'https://www.marriott-ld.com';