import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import App from './containers/App';

import { rootURL } from './config';

import { Provider } from 'mobx-react';
import CaseStore from './stores/CaseStore';
import UserStore from './stores/UserStore';
import SocketStore from './stores/SocketStore';
import ClassStore from './stores/ClassStore';

class Root extends Component {
  render() {
    return (
      <Router basename={rootURL}>
        <Provider
          caseStore={CaseStore}
          userStore={UserStore}
          socketStore={SocketStore}
          classStore={ClassStore}
        >
          <Switch>
            <Route
              path=""
              component={App}
            />
          </Switch>
        </Provider>
      </Router>
    );
  }
}

export default Root;
