import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import ReactGA from 'react-ga';
import { gaId } from '../config';
import Loading from '../components/core/Loading';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import PrivateRoute from '../components/core/PrivateRoute';


import Api from '../api';
import Home from './Home';
import CaseStep from './CaseStep';
import Review from './Review';
import ManageClassList from './ManageClassList';
import ManageClass from './ManageClass';
import ManageSubmission from './ManageSubmission';

const theme = createMuiTheme({
  overrides: {
    MuiBackdrop: {
      root: {
        backgroundColor: 'rgba(0, 0, 0, 0.9)'
      }
    },
    MuiIconButton: {
      root: {
        '&:hover': {
          backgroundColor: 'transparent'
        }
      }
    },
    MuiButton: {
      root: {
        '&$disabled': {
          backgroundColor: '#99A8C3',
          color: '#FFF'
        }
      },
      text: {
        background: 'rgb(85,109,155)',
        borderRadius: 5,
        border: 0,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontSize: '15px',
        letterSpacing: 0,
        lineHeight: '18px',
        height: 40,
        padding: '0 30px',
        '&:hover': {
          backgroundColor: 'rgba(85,109,155,0.6)'
        },
      },
      textPrimary: {
        color: 'white',
      },
      outlined: {
        borderColor: 'rgb(85,109,155)',
        borderWidth: '2px',
        borderRadius: 5,
        color: 'rgb(85,109,155)',
        fontFamily: 'ProximaNova-Regular',
        fontSize: '15px',
        letterSpacing: 0,
        lineHeight: '18px',
        height: 40,
        padding: '0 30px',
        backgroundColor: '#F4F4F4',
        '&:hover': {
          color: '#FFF',
          backgroundColor: '#908FA9'
        },
      }
    },
    MuiStepIcon: {
      root: {
        height: 35,
        width: 35,
        color: '#BFBECD',
        '& $text': {
          fill: '#243961',
        },
        '&$active': {
          color: '#CE722F',
          '& $text': {
            fill: '#FFFFFF',
          }
        },
        '&$completed': {
          color: '#CE722F'
        }
      }
    },
    MuiStepLabel: {
      root: {
        textTransform: 'uppercase',
      },
      label: {
        fontFamily: 'ProximaNova-Regular',
        color: '#FFF',
        opacity: '0.6',
        '&$active': {
          color: '#FFF',
          opacity: '1'
        },
        '&$completed': {
          color: '#FFF'
        }
      }
    },
    MuiStepConnector: {
      vertical: {
        marginLeft: 17,
        padding: '8px 0 8px 0'
      }
    },
    MuiLink: {
      root: {
        fontFamily: 'ProximaNova-Regular',
        cursor: 'pointer'
      }
    },
    MuiFormControl: {
      root: {
        background: '#FFF',
        borderRadius: '4px'
      }
    },
    MuiInputBase: {
      root: {
        outline: '1px solid #BFBECD',
        borderRadius: '4px',
        '&$disabled': {
          background: '#F0F0F0',
        }
      },
      input: {
        color: '#434343',
        '&$disabled': {
          color: '#4F4F4F'
        }
      },
      inputMultiline: {
        // resize: 'vertical',
        minHeight: '76px'
      }
    },
    MuiOutlinedInput: {
      root: {
        '&$focused $notchedOutline': {
          borderColor: '#CE722F',
          borderWidth: '1px',
        }
      },
      // multiline: {
      //   padding: '3px'
      // },
      // inputMultiline: {
      //   padding: '15.5px 11px'
      // }
    },
    MuiTable: {
      root: {
        backgroundColor: '#FFF',
      }
    },
    MuiTableCell: {
      root: {
        paddingTop: '1.5rem',
        paddingBottom: '1.5rem',
        paddingLeft: '2rem',
        paddingRight: '2rem',
        borderBottom: '1px solid #AAA',
      }
    },
    MuiCheckbox: {
      colorSecondary: {
        color: '#FFF',
        '&$checked': {
          color: '#FFF',
        },
        '&$disabled': {
          color: '#BBB',
        },
      }
    },
    MuiFormControlLabel: {
      label: {
        color: '#243961',
        fontFamily: 'ProximaNova-Regular',
        fontSize: '16px'
      }
    },
    MuiPickersDay: {
      isSelected: {
        backgroundColor: 'rgba(85,109,155)',
        '&:hover': {
          backgroundColor: 'rgba(85,109,155,0.6)'
        }
      }
    },
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: 'rgba(85,109,155)'
      }
    },
    MuiTooltip: {
      tooltip: {
        fontSize: '16px',
        color: '#243961',
        backgroundColor: 'white'
      }
    },
    MuiRadio: {
      colorPrimary: {
        color: '#243961'
      },
      colorSecondary: {
        color: '#243961 !important'
      },
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: [
      '"ProximaNova-Regular"',
      '"Roboto"',
      '"Helvetica"',
      '"Arial"',
      'sans-serif'
    ].join(','),
    h1: {
      color: '#243961',
      fontFamily: 'Georgia',
      fontSize: '50px',
      letterSpacing: 0,
      lineHeight: '50px',
      textTransform: 'capitalize'
    },
    h2: {
      color: '#243961',
      fontFamily: 'Georgia',
      fontSize: '18px',
      fontWeight: 700,
      letterSpacing: 0,
      lineHeight: '18px',
      textAlign: 'left'
    },
    h3: {
      color: '#243961',
      fontFamily: 'Calibri',
      fontSize: '16px',
      fontWeight: 700,
      letterSpacing: 0,
      lineHeight: '18px',
      textAlign: 'left'
    },
    h4: {
      color: '#243961',
      fontFamily: 'Calibri',
      fontSize: '12px',
      fontWeight: 700,
      letterSpacing: 0,
      lineHeight: '15px',
      textAlign: 'left'
    },
    h5: {
      color: '#243961',
      fontFamily: 'Calibri',
      fontSize: '18px',
      letterSpacing: 0,
      lineHeight: '18px'
    },
    body1: {
      color: '#000',
      fontFamily: 'Calibri',
      fontSize: '16px',
      letterSpacing: 0,
      lineHeight: '18px',
      textAlign: 'left'
    },
    body2: {
      color: '#000',
      fontFamily: 'Calibri',
      fontSize: '14px',
      fontWeight: 300,
      letterSpacing: 0,
      lineHeight: '14px',
      textAlign: 'left'
    }
  }
});

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      intervalId: null
    }
  }

  componentDidMount() {
    const { userStore, caseStore, classStore } = this.props;
    userStore.fetchUserProfile();
    caseStore.fetchCases();
    classStore.fetchClasses();

    const intervalId = setInterval(() => {
      Api.keepAlive();
    }, 10 * 60 * 1000);

    this.setState({ intervalId });
  }

  componentWillUnmount() {
    const { intervalId } = this.state;
    if (intervalId) {
      clearInterval(intervalId);
      this.setState({ intervalId: null });
    }    
  }

  render() {
    const { userStore, caseStore } = this.props;
    const { isAuthenticated, isActive, ssoUrl } = userStore;

    if (!userStore.isAuthenticated || caseStore.isFetching || userStore.isFetchingUserProfile) {
      return <Loading />;
    }

    if (gaId && userStore.user) {
      ReactGA.initialize(gaId, {
        gaOptions: {
          userId: userStore.user.eid
        }
      });
    }

    return (
      <div>
        <MuiThemeProvider theme={theme}>
          {
            <Switch>
              <PrivateRoute
                path="/submissions"
                component={Review}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
              <PrivateRoute
                exact
                path="/(cases|\/|)/"
                component={Home}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
              <PrivateRoute
                path="/case/:caseId/:stepName/:stepId"
                component={CaseStep}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
              <PrivateRoute
                path="/classes"
                component={ManageClassList}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
              <PrivateRoute
                path="/class/:classId/submission/:stepDataId"
                component={ManageSubmission}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
              <PrivateRoute
                path="/class/:classId/:viewName"
                component={ManageClass}
                isAuthenticated={isAuthenticated}
                isActive={isActive}
                ssoUrl={ssoUrl}
              />
            </Switch>
          }
        </MuiThemeProvider>
      </div >
    );

  }
}

export default inject('userStore', 'caseStore', 'classStore')(observer(App));
