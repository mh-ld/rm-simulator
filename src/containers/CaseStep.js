import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import {
  CssBaseline,
  Paper,
  Typography,
  Link
} from '@material-ui/core';

import Header from '../components/core/Header/Header';
import SideNav from '../components/core/SideNav/SideNav';
import Respond from '../components/steps/AnalyzeDecidePrepare/Respond';
import Record from '../components/steps/Record/Record';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
  paper: {
    backgroundColor: '#BFBECD',
    minHeight: 'calc(100vh - 64px)',
    paddingBottom: theme.spacing.unit * 10,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 12,
    paddingTop: theme.spacing.unit * 6
  },
  back: {
    color: '#000',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
    textAlign: 'right',
    marginTop: '0px',
    marginBottom: theme.spacing.unit * 4
  },
  caseTitle: {
    marginBottom: '1rem'
  },
  stepInstructions: {
    textTransform: 'capitalize'
  }
});

class CaseStep extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navHeader: "", 
      navSubheader: "", 
      navIntro: ""
    }
  }

  async componentDidMount() {
    const { caseId } = this.props.match.params;
    const { cases } = this.props.caseStore;
    
    const currentCase = cases.length > 0 ? cases.find(caseData => caseData._id === caseId) : null;

    this.setState({
      ...this.state,
      navHeader: currentCase.navHeader, 
      navSubheader: currentCase.navSubheader, 
      navIntro: currentCase.navIntro
    });
  }

  renderStep(stepName, stepPropPack) {
    let instruction = '';

    switch (stepName) {
      case 'analyze':
        instruction = 'OLDDocument your analysis process and findings.';
        break;
      case 'decide':
        instruction = 'OLDDocument your decisions and the strategy recommendations you will make.';
        break;
      case 'prepare':
        instruction = 'OLDDocument your plan for presenting your findings and recommendations to the Sales Strategy Team as well as your plan for implementing and critiquing the strategy.';
        break;
      case 'record':
        return <Record {...stepPropPack} />;
      default:
        return null;
    }

    return (
      <Respond 
        {...stepPropPack}
        instruction={instruction}
      />
    );
  }

  render() {
    const { stepName, caseId, stepId } = this.props.match.params;
    const { classes, caseStore, userStore } = this.props;
    const { navHeader, navSubheader, navIntro } = this.state;
    const { user, classId } = userStore;
    const { cases } = caseStore;
    
    const currentCase = cases.length > 0 ? cases.find(caseData => caseData._id === caseId) : null;
    const currentStepIndex = currentCase ? currentCase.steps.findIndex(step => step._id === stepId) : null;
    let backLink = '/cases';
    if (currentStepIndex > 0) {
      const prevStep = currentCase.steps[currentStepIndex-1];
      backLink = `/case/${currentCase._id}/${prevStep.name}/${prevStep._id}`;
    }

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header homeType={'learner'} />
        <SideNav
          currentCase={currentCase}
          currentStepIndex={currentStepIndex}
          learnerView={true}
          navHeader={navHeader}
          navSubheader={navSubheader} 
          navIntro={navIntro}
        />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Paper className={classes.paper}>
            <div>
              <Typography className={classes.back}>
                <Link
                  color="inherit"
                  component={RouterLink}
                  to={backLink}
                >
                  &lt; BACK
                </Link>
              </Typography>
              <Typography variant='h4' className={classes.caseTitle}>
                {currentCase ? "Revenue Management Advanced Strategy + Influence" : null}
              </Typography>
            </div>
            {currentCase && 
              this.renderStep(stepName, {
                caseId: caseId,
                stepId: stepId,
                currentCase: currentCase,
                currentStepIndex: currentStepIndex,
                user: user,
                classId: classId
              })
            }
          </Paper>
        </main>
      </div>
    );
  }
}


export default withStyles(styles)(inject('caseStore', 'userStore')(observer(CaseStep)));