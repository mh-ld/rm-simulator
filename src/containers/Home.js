import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import Main from '../components/core/Main/Main';
import Intro from '../components/core/Intro/Intro';
import Header from '../components/core/Header/Header';

const styles = theme => ({
  root: {
    display: 'block',
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
});

class Home extends Component {
  render() {
    const { classes, caseStore, userStore } = this.props;
    const { cases } = caseStore;


    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Route
            exact
            path="/"
            component={observer((props) =>
              <Intro
                {...props}
                user={userStore.user}
                cases={cases}
                isAdmin={userStore.isAdmin}
                isFacilitator={userStore.isFacilitator}
                isParticipant={userStore.isParticipant}
                classClosed={userStore.classClosed}
                isActive={userStore.isActive}
              />)}
          />
          <Route
            exact
            path="/cases"
            component={observer((props) =>
              <Main
                {...props}
                user={userStore.user}
                cases={cases}
              />)}
          />
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(inject('caseStore', 'userStore')(observer(Home)))