import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import {
  CssBaseline,
  Paper,
  Typography,
  Link
} from '@material-ui/core';

import Header from '../components/core/Header/Header';
import SideNav from '../components/core/SideNav/SideNav';
import ReviewFeedback from '../components/core/Review/ReviewFeedback';
import ReviewMySubmissions from '../components/core/Review/ReviewMySubmissions';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
  paper: {
    backgroundColor: '#BFBECD',
    minHeight: 'calc(100vh - 64px)',
    paddingBottom: theme.spacing.unit * 10,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 12,
    paddingTop: theme.spacing.unit * 6
  },
  back: {
    color: '#000',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
    textAlign: 'right',
    marginTop: '0px',
    marginBottom: theme.spacing.unit * 4
  }
});

class Review extends Component {

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header homeType={'learner'} />
        <SideNav
          currentCase={null}
          currentStepIndex={null}
          drawerOpen={true}
          learnerView={false}
        />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Paper className={classes.paper}>
            <div>
                <Typography className={classes.back}>
                  <Link
                    color="inherit"
                    component={RouterLink}
                    to="/cases"
                  >
                    &lt; BACK
                  </Link>
                </Typography>
              </div>
            <Route
              exact
              path="/submissions"
              component={ReviewMySubmissions}
            />
            <Route
              exact
              path="/submissions/:stepDataId/:subIndex"
              component={ReviewFeedback}
            />
          </Paper>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(Review)