import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import {
  CssBaseline,
  Paper,
  Typography,
  Link
} from '@material-ui/core';

import Header from '../components/core/Header/Header';
import SideNav from '../components/core/SideNav/SideNav';
import ClassList from '../components/core/ClassList/ClassList';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
  paper: {
    backgroundColor: '#BFBECD',
    minHeight: 'calc(100vh - 64px)',
    paddingBottom: theme.spacing.unit * 10,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 12,
    paddingTop: theme.spacing.unit * 6
  },
  back: {
    color: '#000',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
    textAlign: 'right',
    marginTop: '0px',
    marginBottom: theme.spacing.unit * 4
  }
});

class ManageClassList extends Component {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    const { classes } = this.props; 

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header />
        <SideNav
          currentCase={null}
          currentStepIndex={null}
          drawerOpen={true}
          learnerView={false}
        />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Paper className={classes.paper}>
            <div>
              <Typography className={classes.back}>
                <Link
                  color="inherit"
                  component={RouterLink}
                  to="/"
                >
                  &lt; BACK
                </Link>
              </Typography>
            </div>
            <ClassList
              userStore={this.props.userStore}
            />
          </Paper>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(inject('caseStore', 'userStore')(observer(ManageClassList)));