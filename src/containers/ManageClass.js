import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import {
  CssBaseline,
  Paper,
  Typography,
  Link,
  Grid
} from '@material-ui/core';

import Header from '../components/core/Header/Header';
import SideNav from '../components/core/SideNav/SideNav';
import ClassSubmissions from '../components/core/ClassDetails/ClassSubmissions';
import EditClass from '../components/core/ClassDetails/EditClass';
import Loading from '../components/core/Loading'

import CECSimApi from '../api';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
  paper: {
    backgroundColor: '#BFBECD',
    minHeight: 'calc(100vh - 64px)',
    paddingBottom: theme.spacing.unit * 10,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 12,
    paddingTop: theme.spacing.unit * 6
  },
  back: {
    color: '#000',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
    textAlign: 'right',
    marginTop: '0px',
    marginBottom: theme.spacing.unit * 4
  },
  switchView: {
    marginBottom: '-2rem',
  }, 
  switchViewLink: {
    zIndex: 999,
    color: '#243961'
  }
});

class ManageClass extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      currentClass: null
    }
  }

  async componentDidMount() {
    if (this.props.match.params.classId !== 'new') {
      const {data: currentClass} = await CECSimApi.getClass(this.props.match.params.classId);

      this.setState({ 
        currentClass: currentClass
      });
    }
  }

  switchClassView = () => {
    const { classId, viewName } = this.props.match.params;
    this.props.history.push(`/class/${classId}/${viewName === 'edit' ? 'submissions' : 'edit'}`);
  }

  render() {
    const { classId, viewName } = this.props.match.params;
    const { classes, userStore } = this.props;
    const { currentClass } = this.state;
    const { isAdmin, isFacilitator } = userStore;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header homeType={'facilitator'} />
        <SideNav
          currentCase={null}
          currentStepIndex={null}
          drawerOpen={true}
          learnerView={false}
        />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Paper className={classes.paper}>
            <div>
              <Typography className={classes.back}>
                <Link
                  color="inherit"
                  component={RouterLink}
                  to="/classes"
                >
                  &lt; BACK
                </Link>
              </Typography>
            </div>
            { classId !== 'new' && isAdmin && isFacilitator ? 
              <Grid container justify='flex-end' className={classes.switchView}>
                <Link onClick={this.switchClassView} className={classes.switchViewLink} underline='always'>
                  {viewName === 'edit' ? 'Switch to Facilitator View' : 'Switch to Admin View'}
                </Link>
              </Grid> : null
            }
            {(classId === 'new' || viewName === 'edit') && 
              <EditClass editType={classId === 'new' ? 'new' : 'edit'} />
            }
            {viewName === 'submissions' && 
              (currentClass ?
                <ClassSubmissions 
                  name={currentClass.name}
                  startDate={currentClass.startDate}
                  participants={currentClass.participants}
                />
              :
                <Loading />
              )
            }
          </Paper>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(inject('caseStore', 'userStore')(observer(ManageClass)));