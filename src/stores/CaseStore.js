import { decorate, observable, computed, action } from 'mobx';
import CECSimApi from '../api';

class CaseStore {
  rawCases = [];
  isFetching = false;

  async fetchCases({silent = false} = {}) {
    if(!silent) {
      this.isFetching = true;
    }

    const { data } = await CECSimApi.getCases();
    this.rawCases = data;

    this.isFetching = false;
    
  }

  addPriorStepCompleted(rawCases) {
    rawCases.forEach((rawCase) => {
      if(rawCase.steps) {
        let priorStepCompleted = false;
        rawCase.steps.forEach((step) => {
          if(step.completed) {
            priorStepCompleted = true;
            step.priorStepCompleted = true;
          } else {
            step.priorStepCompleted = priorStepCompleted;
            priorStepCompleted = false;
          }
        })
      }
    });

    return rawCases;
  }

  addStepsCompleted(rawCases) {
    return rawCases.map((rawCase) => {
      if(rawCase.steps) {
        return {
          ...rawCase,
          stepsCompleted: rawCase.steps.reduce((stepsAcc, step) => step.completed ? stepsAcc + 1 : stepsAcc, 0)
        }
      } else {
        return { ...rawCase, stepsCompleted: 0}
      }
    });
  }

  get cases() {
    let cases = this.addStepsCompleted(this.rawCases);
    cases = this.addPriorStepCompleted(cases);
    return cases;
  }
}

decorate(CaseStore, {
  fetchCases: action,
  rawCases: observable,
  cases: computed,
  isFetching: observable,
})

export default new CaseStore();