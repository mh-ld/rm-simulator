import { decorate, observable, action, computed } from 'mobx';
import CECSimApi from '../api';

class ClassStore {
  rawClasses = [];

  isFetching = false;

  async fetchClasses({silent = false} = {}) {
    if(!silent) {
      this.isFetching = true;
    }

    const { data } = await CECSimApi.getClasses();

    this.rawClasses = data;
    this.isFetching = false;
  }

  get rmClasses() {
    return this.rawClasses.map((rawClass) => { return { ...rawClass, } });
  }

}

decorate(ClassStore, {
  fetchClasses: action,
  rawClasses: observable,
  rmClasses: computed
})

export default new ClassStore();