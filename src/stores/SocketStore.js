import { decorate, observable, computed, action } from 'mobx';
import { socketURL } from '../config';
import io from 'socket.io-client';


class SocketStore {
  socket = null;
  messages = [];
  isConnected = false;
  started = false;
  secondScreenConnected = false;

  connect(channel, {secondScreen = false} = {}) {
    this.socket = io(socketURL);

    this.socket.on('connect', () => {
      this.socket.emit('room', channel);
      this.isConnected = true;

      if(secondScreen) {
        this.socket.emit('message', {secondScreenConnected: true});
      }
      console.log('socket connected');
    });

    this.socket.on('message', (message) => {
      console.log('message', message);
      if(message.secondScreenConnected) {
        this.secondScreenConnected = true;
      } else if(message.started) {
        this.started = true
      } else {
        this.messages = [...this.messages, message];
        console.log('pushing message', this.messages);
      }
    });
  }

  get lastMessage() {
    console.log('lastMessage', this.messages[this.messages.length-1]);
    return this.messages[this.messages.length-1];
  }

  sendMessage(message) {
    if(this.socket && this.socket.emit) {
      this.socket.emit('message', message);
    } else {
      console.error('no socket exists');
    }
  }
}

decorate(SocketStore, {
  connect: action,
  messages: observable,
  lastMessage: computed,
  sendMessage: action,
  secondScreenConnected: observable,
  started: observable
});

export default new SocketStore();