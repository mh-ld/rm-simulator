import { decorate, observable, action } from 'mobx';
import Api from '../api';
import authStore from './AuthStore';

class UserStore {
  user = null;
  classClosed = true;
  classId = "";
  isFetchingStepDatas = false;
  isFetchingUserProfile = false;
  isAuthenticated = false;
  isAdmin = false;
  stepDatas = null;
  isFacilitator = false;
  isParticipant = false;
  ssoUrl = null;
  isActive = false;

  setUserProfile(user) {
    this.isAuthenticated = true;
    this.user = user;
    authStore.start(); // start tracking auth session
  }

  async fetchClassData() {
    const { data: rmClass } = await Api.getUserClass();

    this.classClosed = Object.keys(rmClass).length !== 0 ? rmClass.closed : true;
    this.classId = Object.keys(rmClass).length !== 0 ? rmClass._id : "";
  }

  async fetchStepDatas() {
    this.isFetchingStepDatas = true;
    const { data: stepDatas } = await Api.getAllUserStepData(this.user.eid);
    
    this.stepDatas = stepDatas;
    this.isFetchingStepDatas = false;
  }

  async fetchUserProfile() {
    if (this.user !== null) {
      return;
    }

    try {
      this.isFetchingUserProfile = true;
      const { status, data: user } = await Api.getUserProfile();

      if (status === 401 || !user) {
        this.isFetchingUserProfile = false;
        this.isAuthenticated = false;
        if (status === 401 && user) this.ssoUrl = user.ssoUrl;
        return;
      }

      this.setUserProfile(user);

      const { data: roles } = await Api.getUserRoles();
      this.isAdmin = roles.admin;
      this.isFacilitator = roles.facilitator;
      this.isParticipant = roles.participant;

      const superAdmins = ['nstri879', 'dpate818'];
      if (superAdmins.includes(user.eid)) {
        this.isAdmin = true;
        this.isFacilitator = true;
        this.isParticipant = true;
      }

      if (this.isParticipant) {
        await this.fetchClassData();
      }

      this.isActive = this.isAdmin || this.isFacilitator || (this.isParticipant && !this.classClosed);

      this.isFetchingUserProfile = false;
    }
    catch (e) {
      this.isFetchingUserProfile = false;
      this.isAuthenticated = false;
    }
  }

  getStepData(caseId, stepId) {
    if (!this.isParticipant || !this.stepDatas) {
      console.log('Step data not found');
      return null;
    }
    return this.stepDatas.find(sd => sd.case._id === caseId && sd.step._id === stepId);
  }

  getStepDataById(stepDataId) {
    if (!this.isParticipant || !this.stepDatas) {
      console.log('Step data not found');
      return null;
    }
    return this.stepDatas.find(sd => sd._id === stepDataId);
  }
}

decorate(UserStore, {
  user: observable,
  isFetchingUserProfile: observable,
  isFetchingStepDatas: observable,
  isAuthenticated: observable,
  isActive: observable,
  isAdmin: observable,
  isFacilitator: observable,
  isParticipant: observable,
  ssoUrl: observable,
  fetchUserProfile: action,
  fetchStepDatas: action,
  login: action,
  logout: action,
  classClosed: observable,
  classId: observable,
  stepDatas: observable,
  getStepData: action,
  getStepDataById: action
});

export default new UserStore();
