import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';

import {
  Grid,
  Typography,
  Button,
  Link,
  IconButton
} from '@material-ui/core';
import { 
  DeleteOutlined, 
  ExpandLessRounded, 
  ExpandMoreRounded
} from '@material-ui/icons';

import FinishedModal from '../Shared/Modals/FinishedModal';
import IntroResources from '../Shared/IntroResources';
// import HorizontalRule from '../Shared/HorizontalRule';
import Recorder from './Recorder';
import Loading from '../../core/Loading'

import CECSimApi from '../../../api';
import { apiURL } from '../../../config';

const styles = theme => ({
  paragraph: {
    marginBottom: '1rem'
  },
  horizontalRuleGrid: {
    marginTop: '2rem',
    marginBottom: '2rem'
  },
  videoContainer: {
    marginTop: '1rem',
    marginBottom: '2rem'
  },
  summary: {
    fontStyle: 'italic',
    marginTop: '1rem'
  },
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    width: '100%',
    color: '#FFFFFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  },
  orangeButton: {
    backgroundColor: '#CE722F',
  },
  error: {
    marginTop: '1rem',
    color: 'red',
  },
  sectionHeader: {
    marginTop: '1rem',
    marginBottom: '1rem'
  },
  changeStratGrid: {
    marginTop: '1rem'
  },
  changeStratLink: {
    color: 'black',
    textDecoration: 'underline',
    fontSize: '14px',
    fontStyle: 'italic'
  },
  dropdownContainer: {
    background: '#fff',
    borderRadius: '5px',
    marginTop: '2rem',
    marginBottom: '1rem'
  },
  dropdownHeader: {
    padding: '1rem',
    cursor: 'pointer'
  },
  dropdownContent: {
    marginTop: '-1rem',
    padding: '1rem'
  },
  dropdownParagraph: {
    color: '#434343',
    lineHeight: '20px',
    marginTop: '1rem',
    marginBottom: '1rem'
  },
  recordButton: {
    marginTop: '1rem',
    marginBottom: '1rem',
    minWidth: '230px',
    color: '#FFFFFF',
    fontFamily: 'ProximaNova-Regular',
    textTransform: 'none',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  }
});

class Record extends Component {

  constructor(props) {
    super(props);

    this.state = {
      summary: '',
      descriptions: [],
      notes: [],
      bullets: [],
      files: [],
      inputData: {},
      prevInputData: {},
      showFinishedModal: false,
      emptyFields: false,
      stepDisabled: true,
      videoStrategy: 'upload',
      validExts: {
        recording: ['mkv','mov','mp4','webm'],
        ppt: ['ppt','pptx']
      },
      error: {},
      uploadingRec: false,
      uploadingPPT: false,
      isLoading: true,
      showDropdown: false
    };
  }

  async componentDidMount() {
    const { caseId, stepId, user, caseStore } = this.props;
    this.setState({ isLoading: true });

    const { data: caseStep } = await CECSimApi.getStep(caseId, stepId);
    const { data: stepData } = await CECSimApi.getUserStepData(user.eid, caseId, stepId);
    const newState = { ...caseStep.data };

    if (stepData) {
      if (stepData.submissions.length > 0) {
        const numSubs = stepData.submissions.length;
        const lastSub = stepData.submissions[numSubs-1];

        newState.inputData = lastSub.data || {};
      }
      newState.stepDisabled = stepData.status === 'Needs Review' || stepData.status === 'Complete' || caseStore.cases.find(c => c._id === caseId).stepsCompleted < stepData.step.order - 1;
      newState.popupHasShown = !!stepData.popupHasShown;

    } else {
      const { data: enabled } = await CECSimApi.checkUserCanEdit(user.eid, caseId, stepId);
      newState.stepDisabled = !enabled;

      if (enabled) {
        await CECSimApi.createStepData(caseId, stepId);
      }
    }

    this.setState({ ...newState, isLoading: false });
  }

  onSwitchVideoStrategy = async () => {
    const { videoStrategy } = this.state;
    this.setState({ videoStrategy: videoStrategy === 'upload' ? 'record' : 'upload' });
  }

  handleUpload = async (event, type) => {
    const { inputData, validExts, error } = this.state;
    const file = event.target.files[0];
    if (!file) return;

    const fileExt = file.name.split('.')[1].toLowerCase();

    // Check for valid file type
    if (validExts[type].includes(fileExt)) {
      this.setState({ error: { ...error, [type]: null } });
    } else {
      this.setState({ error: {
        ...error, 
        [type]: `File type ".${fileExt}" not supported. Supported file types: ${validExts[type].map(ext => '.' + ext).join(', ')}`
      }});
      return;
    }

    const uploadToS3 = async () => {
      try {
        if (type === 'recording') {
          this.setState({ uploadingRec: true });
        } else {
          this.setState({ uploadingPPT: true });
        }
        const form = new FormData();
        form.append(type, file);
  
        const { data } = await CECSimApi.uploadRMRecording(form);
  
        const newInputData = inputData;
        if (type === 'recording') {
          newInputData.compositionIds = [data.fileName];
        } else {
          newInputData.ppt= {
            name: file.name,
            fileName: data.fileName
          }
        }
        this.setState({ inputData: newInputData }); 
        if (type === 'recording') {
          this.setState({ uploadingRec: false });
        } else {
          this.setState({ uploadingPPT: false });
        }
      } catch (e) {
        console.log(e);
        if (type === 'recording') {
          this.setState({ uploadingRec: false });
        } else {
          this.setState({ uploadingPPT: false });
        }
      }
    }

    if (type === 'recording') {
      // Check recording length < 5 min
      try {
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = async () => {
          const duration = video.duration;
          window.URL.revokeObjectURL(video.src);

          if (duration > 300) {
            this.setState({ error: { ...error, recording: `Video must be no more than 5 minutes long.` } });
            return;
          } else {
            await uploadToS3();
          }
        };

        video.src = URL.createObjectURL(file);
      } catch (e) {
        console.log(e);
      }
    } else {
      await uploadToS3();
    }
  }

  onDownload(fileName) {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
        const blobUrl = window.URL.createObjectURL(xmlHttp.response);
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = fileName;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    };
    xmlHttp.responseType = 'blob';
    xmlHttp.open('GET', `${apiURL}/rm/recording/${fileName}`, true);
    xmlHttp.send(null);
  }

  onDeletePPT = () => {
    const { inputData } = this.state;
    const { ppt: _, ...newInputData } = inputData;

    this.setState({ inputData: newInputData }); 
  }

  onSaveRecording = async (compositionId) => {
    const { caseId, stepId } = this.props;
    const { popupHasShown, inputData } = this.state;
    const newInputData = { 
      ...inputData,
      compositionIds: [compositionId] 
    };

    await CECSimApi.createSubmission(caseId, stepId, newInputData, popupHasShown, false);
    
    this.setState({
      inputData: newInputData,
      videoStrategy: 'upload'
    });
  }

  onSave = async () => {
    const { caseId, stepId } = this.props;
    const { inputData, popupHasShown } = this.state;

    await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, false);
  }

  onSaveAndSubmit = async () => {
    const { caseId, stepId, caseStore } = this.props;
    const { inputData, popupHasShown } = this.state;

    if (this.noEmptyFields()) {
      this.setState({ stepDisabled: true });
      await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, true);
      caseStore.fetchCases({ silent: true });
      this.setState({ showFinishedModal: true });
    } else {
      await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, false);
    }
  }

  noEmptyFields = () => {
    const { inputData } = this.state;
    const emptyFields = !inputData || !inputData.compositionIds || inputData.compositionIds.length === 0;

    this.setState({ emptyFields: emptyFields });
    return !emptyFields;
  }

  onFinishedModalClosed = async () => {
    this.props.history.push('/cases');
  }

  toggleDropdown = () => {
    const { showDropdown } = this.state;
    this.setState({ showDropdown: !showDropdown });
  }

  render() {
    const { classes, userStore, currentCase, currentStepIndex } = this.props;
    const { summary, files, inputData, stepDisabled, showFinishedModal, emptyFields, videoStrategy, validExts, error, uploadingRec, uploadingPPT, isLoading, notes, showDropdown } = this.state;

    return (!isLoading ?
      <Grid container>
        <IntroResources 
          descriptions={this.state.descriptions}
          bullets={this.state.bullets}
          caseNum={currentCase.order}
          stepNum={currentStepIndex+1}
          stepName={currentCase.steps[currentStepIndex].title}
          files={files}
        />
        {/* <HorizontalRule /> */}

        {/* Video Section */}
        <Grid container item xs={12}>
          <Grid item xs={12} className={classes.sectionHeader}>
            <Typography variant='h3'>Video</Typography>
          </Grid>

          {stepDisabled ? (
            (inputData && inputData.compositionIds && 
              inputData.compositionIds.map(fileName => (
                <Grid item container xs={12} justify="center" className={classes.videoContainer} key={fileName}>
                  <video controls width="800" height="450" src={`${apiURL}/rm/recording/${fileName}`} type="video/webm" />
                </Grid>
              ))
            )
          ) : (
            <Grid container item xs={12}>
              {videoStrategy === 'upload' ? (
                <Grid item xs={12}>
                  <input
                    type='file'
                    onChange={(e) => this.handleUpload(e, 'recording')}
                    accept={validExts.recording.map(ext => '.' + ext).join(',')}
                  />
                  {error.recording && 
                    <Typography variant='body1' className={classes.error}>
                      {error.recording}
                    </Typography>
                  }
                  {!uploadingRec ?
                    <Grid item xs={12}>
                      {inputData && inputData.compositionIds && 
                        inputData.compositionIds.map(fileName => (
                          <Grid item container xs={12} justify="center" className={classes.videoContainer} key={fileName}>
                            <video controls width="800" height="450" src={`${apiURL}/rm/recording/${fileName}`} type="video/webm" />
                          </Grid>
                        ))
                      }
                    </Grid>
                  :
                    <Loading />
                  }
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <Recorder
                    userStore={userStore}
                    onSaveRecording={this.onSaveRecording}
                  />
                  <Grid item xs={12} className={classes.changeStratGrid}>
                    <Link className={classes.changeStratLink} onClick={this.onSwitchVideoStrategy}>
                      Cancel Recording In-browser
                    </Link>
                  </Grid>
                </Grid>
              )}
            </Grid>
          )}
        </Grid>

        {/* Can't Record in Teams Dropdown */}
        <Grid container item className={classes.dropdownContainer}>
          <Grid container item direction='row' justify='space-between' className={classes.dropdownHeader} onClick={this.toggleDropdown}>
            <Typography variant="h3" style={{color: '#355894', paddingTop: '4px'}}>
              Cannot Record in Teams?
            </Typography>
            <ExpandMoreRounded className={classes.icon} style={{display: showDropdown && 'none'}}/>
            <ExpandLessRounded className={classes.icon} style={{display: !showDropdown && 'none'}}/>
          </Grid>
          <Grid item className={classes.dropdownContent} style={{display: !showDropdown && 'none'}}>
            {/* Text */}
            <Typography variant='h5' style={{color: '#355894'}}>
              RECORD DIRECTLY IN THE LEARNING PORTAL
            </Typography>
            {notes && notes.length > 0 && 
              <Grid>
                {notes.map((text, index) => {
                  let position = text.indexOf('click here');

                  const paragraphElement = (position < 0) ? 
                    (
                      <>{text}</>
                    ) : (
                      <>
                        {text.substring(0, position+6)}
                        <Link
                          href='https://support.microsoft.com/en-us/office/record-a-meeting-in-teams-34dfbe7f-b07d-4a27-b4c6-de62f1348c24'
                          underline='always'
                          target='_blank'
                          rel='noopener noreferrer'
                        >here</Link>
                        {text.substring(position+10)}
                      </>
                    );

                  return (
                    <Typography
                      variant='body1'
                      className={classes.dropdownParagraph}
                      key={index}
                      style={index === notes.length-1 ? {fontStyle: 'italic'} : {}}
                    >
                      {paragraphElement}
                    </Typography>
                  )
                })}
              </Grid>
            }

            {/* Record Button */}
            <Grid item className={classes.changeStratGrid}>
              <Button disabled={stepDisabled || uploadingRec || videoStrategy !== 'upload'} className={classes.recordButton} onClick={this.onSwitchVideoStrategy}>
                Record in-browser
              </Button>
            </Grid>

            {/* PPT Section */}
            <Grid container item xs={12}>
              <Grid item xs={12} className={classes.sectionHeader}>
                <Typography variant='h3' style={{color: '#355894'}}>Powerpoint Presentation (optional)</Typography>
              </Grid>
              {!stepDisabled &&
                <Grid item xs={12}>
                  <input
                    type="file"
                    onChange={(e) => this.handleUpload(e, 'ppt')}
                    accept={validExts.ppt.map(ext => '.' + ext).join(',')}
                  />
                </Grid>
              }
              {error.ppt && 
                <Typography variant='body1' className={classes.error}>
                  {error.ppt}
                </Typography>
              }
              {!uploadingPPT ?
                <Grid item xs={12}>
                  {inputData && inputData.ppt &&
                    <Grid item xs={12}>
                      <Link onClick={this.onDownload.bind(this, inputData.ppt.fileName)}>
                        {inputData.ppt.name}
                      </Link>
                      <IconButton onClick={this.onDeletePPT}>
                        <DeleteOutlined />
                      </IconButton>
                    </Grid>
                  }
                </Grid>
              :
                <Loading />
              }
            </Grid>
          </Grid>
        </Grid>

        {/* Bottom Section */}
        <Grid container className={classes.buttonGrid}>
          <Grid item xs={8}>
            {summary &&
              <Typography variant='body1' className={classes.summary}>
                {summary}
              </Typography>
            }
          </Grid>
          <Grid container item xs={4}>
            <Grid container justify="flex-end">
              <Grid item>
                <Button disabled={stepDisabled || uploadingRec || uploadingPPT} className={classes.button} onClick={this.onSave}>
                  Save
                </Button>
                <Button disabled={stepDisabled || uploadingRec || uploadingPPT} className={classNames(classes.button, classes.orangeButton)} onClick={this.onSaveAndSubmit}>
                  Complete
                </Button>
                {emptyFields &&
                  <Typography variant='body1' className={classes.error}>
                    You must upload a video.
                  </Typography>
                }
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {showFinishedModal &&
          <FinishedModal
            handleClose={this.onFinishedModalClosed}
            currentCase={currentCase}
            currentStepIndex={currentStepIndex}
            open={showFinishedModal}
            buttonText="Submit and Return to Home"
          />
        }
      </Grid>
    : 
      <Loading />
    );
  }
}

export default withRouter(withStyles(styles)(inject('caseStore', 'userStore')(observer(Record))));