import React, { Component, useEffect, useRef } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { ReactMediaRecorder } from 'react-media-recorder';

import StartRecordIcon from './StartRecordIcon';
import StopRecordIcon from './StopIcon';

import {
  Grid,
  Typography,
  IconButton,
  Button
} from '@material-ui/core';

import hasVideoRecordingDevice from '../../../utilities/hasVideoRecordingDevice';

import Loading from './Loading';
import CECSimApi from '../../../api';

const styles = theme => ({
  rootGrid: {
  },
  forwardIcon: {
    fontSize: '2rem',
    paddingLeft: '.5rem'
  },
  textAlignCenter: {
    textAlign: 'center'
  },
  cursorPointer: {
    cursor: 'pointer'
  },
  redoButton: {
    border: '1px solid #000',
    borderRadius: 26,
    height: 55,
    width: 55,
    backgroundColor: 'transparent',
    textTransform: 'none',
    verticalAlign: 'super',
    padding: '0 25px',
    minWidth: 52,
    marginRight: 15,
    color: '#000',
    '&:hover': {
      backgroundColor: 'transparent'
    }
  },
  redoButtonGrid: {
    marginTop: 20
  },
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    width: '100%',
  },
  orangeButton: {
    backgroundColor: '#CE722F',
  },
  error: {
    marginTop: '1rem',
    color: 'red',
  }
});


const LoadingWithGrid = ({ classes }) => (
  <Grid container justify="center">
    <Grid item xs={12}>
      <Loading />
    </Grid>
  </Grid>
);

class Recorder extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isRecording: false,
      recordingComplete: false,
      redo: false,
      uploading: false,
      finishedStep: false,
      startTime: null,
      tooLong: false
    };
  }

  async componentDidMount() {
    this.startPreview();
  }

  startPreview = async () => {
    const hasCam = await hasVideoRecordingDevice();

    if (!hasCam) {
      console.log('no camera!');
    }
  }

  wait = async (ms) => {
    return new Promise(resolve => {
      setTimeout(resolve, ms);
    });
  }

  onToggleRecording = () => {
    const { isRecording } = this.state;

    if (!isRecording) {
      this.setState({ isRecording: true });
    } else {
      this.setState({ isRecording: false, recordingComplete: true });
    }
  }

  onRedoClick = async () => {
    this.setState({
      redo: true,
      isRecording: false,
      recordingComplete: false,
      uploading: false,
      tooLong: false
    });

    this.startPreview();

    await this.wait(500);

    this.setState({ redo: false });
  }

  startTimer = async () => {
    this.setState({ startTime: new Date() });
  }

  stopTimer = async () => {
    const { startTime } = this.state;
    const stopTime = new Date();
    const duration = (stopTime - startTime) / 1000;

    this.setState({ tooLong: duration > 300 });
  }

  onSaveRecording = async mediaBlobUrl => {
    const blob = await fetch(mediaBlobUrl).then(r => r.blob());

    const file = new File(
      [blob],
      "file.webm",
      { type: 'video/webm' }
    );

    const form = new FormData();
    form.append('recording', file);

    this.setState({ uploading: true, tooLong: false });
    const { data } = await CECSimApi.uploadRMRecording(form);
    this.setState({ uploading: false, finishedStep: true });
    this.props.onSaveRecording(data.fileName);
  }

  render() {
    const { classes } = this.props;
    const { isRecording, recordingComplete, redo, uploading, finishedStep, tooLong } = this.state;

    const RecordingStream = ({ status, startRecording, stopRecording, mediaBlobUrl, previewStream }) => {
      const videoRef = useRef(null);

      useEffect(() => {
        if (videoRef.current && previewStream) {
          videoRef.current.srcObject = previewStream;
        }
      }, [previewStream]);

      useEffect(() => {
        if (isRecording && !recordingComplete) {
          startRecording();
          this.startTimer();
        }

        if (!isRecording && recordingComplete) {
          stopRecording();
          this.stopTimer();
        }

      }, [isRecording, recordingComplete]);

      console.log(status);

      if (!previewStream && !recordingComplete) {
        return (
          <LoadingWithGrid classes={classes} />
        );
      }

      return (
        <Grid item justify="center" container xs={12}>
          <video hidden={!recordingComplete} src={mediaBlobUrl} controls width={800} height={450} />
          <video hidden={recordingComplete} ref={videoRef} width={800} height={450} autoPlay />

          {recordingComplete ? (
            <Grid container className={classes.buttonGrid}>
              <Grid item xs={8}></Grid>
              <Grid container item xs={4}>
                <Grid container>
                  <Grid item>
                    {tooLong && 
                      <Typography variant='body1' className={classes.error}>
                        Video must be no more than 5 minutes long.
                      </Typography>
                    }
                    <Button variant='outlined' className={classes.button} onClick={this.onRedoClick}>
                      Redo
                    </Button>
                    <Button className={classes.button} onClick={this.onSaveRecording.bind(this, mediaBlobUrl)} disabled={tooLong}>
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          ) : (
            <Grid item container xs={12}>
              <Grid item xs={12} className={classes.textAlignCenter}>
                <IconButton onClick={this.onToggleRecording}>
                  {isRecording ? <StopRecordIcon color="#000" /> : <StartRecordIcon color="#000" />}
                </IconButton>
              </Grid>
              <Grid item xs={12} className={classes.textAlignCenter}>
                <Typography align='center'>{isRecording ? "Stop Recording" : "Begin Recording"}</Typography>
              </Grid>
            </Grid>
          )
          }
        </Grid>
      )
    };

    if (uploading) {
      return (
        <LoadingWithGrid classes={classes} />
      )
    }

    return (
      <Grid container justify="center" className={classes.rootGrid}>
        <Grid item xs={12}>
          {!redo && !finishedStep && (
            <ReactMediaRecorder
              video
              askPermissionOnMount
              render={RecordingStream}
            />
          )}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Recorder);