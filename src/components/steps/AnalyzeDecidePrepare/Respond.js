import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import ReactToPrint from 'react-to-print';
import classNames from 'classnames';
import _ from 'lodash';

import {
  Grid,
  Typography,
  Button,
  IconButton
} from '@material-ui/core';
import {
  PrintOutlined
} from '@material-ui/icons';

import PopupModal from '../Shared/Modals/PopupModal';
import FinishedModal from '../Shared/Modals/FinishedModal';
import IntroResources from '../Shared/IntroResources';
import HorizontalRule from '../Shared/HorizontalRule';
import Input from '../Shared/Input';
import Loading from '../../core/Loading'
import Header from '../../core/Header/Header'

import CECSimApi from '../../../api';

const styles = theme => ({
  summary: {
    fontStyle: 'italic',
    marginTop: '1rem'
  },
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    width: '100%',
    color: '#FFFFFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  },
  orangeButton: {
    backgroundColor: '#CE722F',
  },
  error: {
    marginTop: '1rem',
    color: 'red',
  },
  sectionHeader: {
    marginTop: '2rem',
    marginBottom: '-1rem'
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#243961'
  },
  printIconGrid: {
    marginTop: '-81px'
  },
  printableWrapper: {
    display: 'none',
  },
  stepHeader: {
    textTransform: 'uppercase',
    marginBottom: '2rem'
  },
  submissionItem: {
    marginTop: '1rem'
  },
  question: {
    fontStyle: 'italic'
  },
  response: {
    marginTop: '.5rem',
    color: '#434343',
    whiteSpace: 'pre-wrap'
  },
  '@media print': {
    printableWrapper: {
      display: 'flex',
    },
    printableContainer: {
      marginTop: '64px',
      padding: '20px'
    },
    indentOnPrint: {
      paddingLeft: '12px'
    },
    stepHeader: {
      marginTop: '1rem',
      marginBottom: '1rem'
    },
    submissionContainer: {
      marginTop: '-.5rem',
    },
    sectionHeader: {
      fontStyle: 'italic',
      fontWeight: '700',
      marginTop: '0',
      marginBottom: '0'
    },
    textLabel: {
      marginBottom: '0px'
    },
    prevTextLabel: {
      fontStyle: 'italic',
      fontWeight: 300,
      marginTop: '1rem'
    }
  }
});

const initialState = {
  summary: '',
  descriptions: [],
  notes: [],
  bullets: [],
  files: [],
  sections: [],
  inputs: [],
  inputData: {},
  prevInputData: {},
  popop: null,
  popupHasShown: false,
  showPopupModal: false,
  showFinishedModal: false,
  emptyFields: false,
  stepDisabled: true,
  submissionType: 'none',
  isLoading: true
}

class Respond extends Component {
  constructor(props) {
    super(props);

    this.state = initialState;
  }

  async initializeStepData() {
    const { caseId, stepId, user, caseStore } = this.props;
    
    this.setState({ ...initialState });

    const { data: caseStep } = await CECSimApi.getStep(caseId, stepId);
    const { data: stepData } = await CECSimApi.getUserStepData(user.eid, caseId, stepId);
    const newState = { ...caseStep.data };

    if (newState.submissionType === 'none') {
      newState.inputData = {};
      if ('inputs' in newState) {
        newState.inputs.forEach(input => {
          newState.inputData[input.name] = input.answer;
        });
      } else if ('sections' in newState) {
        newState.sections.forEach(section => {
          section.inputs.forEach(input => {
            newState.inputData[input.name] = input.answer;
          })
        });
      }
    }

    if (stepData) {
      if (newState.submissionType !== 'none' && stepData.submissions.length > 0) {
        const numSubs = stepData.submissions.length;
        const lastSub = stepData.submissions[numSubs-1];

        if (lastSub.data) {
          newState.inputData = lastSub.data;
        } else {
          newState.inputData = {};
          if ('inputs' in newState) {
            newState.inputs.forEach(input => {
              newState.inputData[input.name] = "";
            });
          } else if ('sections' in newState) {
            newState.sections.forEach(section => {
              section.inputs.forEach(input => {
                newState.inputData[input.name] = "";
              })
            });
          }
          console.log(newState.inputData);
        }

        if (lastSub.submitted && !(_.get(lastSub, 'feedback.approved', true))) {
          newState.prevInputData = lastSub.data;
        } else if (numSubs > 1){
          newState.prevInputData = stepData.submissions[numSubs-2].data;
        }
      }
      newState.stepDisabled = stepData.status === 'Needs Review' || stepData.status === 'Complete' || caseStore.cases.find(c => c._id === caseId).stepsCompleted < stepData.step.order - 1;
      newState.popupHasShown = !!stepData.popupHasShown;
    } else {
      const { data: enabled } = await CECSimApi.checkUserCanEdit(user.eid, caseId, stepId);
      newState.stepDisabled = !enabled;

      if (enabled) {
        await CECSimApi.createStepData(caseId, stepId);
      }
    }

    this.setState({ ...newState, isLoading: false });
  }

  async componentDidMount() {
    await this.initializeStepData();
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.stepId !== this.props.stepId || prevProps.caseId !== this.props.caseId) {
      await this.initializeStepData();
    }
  }

  onSave = async () => {
    const { caseId, stepId } = this.props;
    const { inputData, popupHasShown } = this.state;

    await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, false);
  }

  onSaveAndSubmit = async () => {
    const { caseId, stepId, caseStore } = this.props;
    const { inputData, popupHasShown, submissionType } = this.state;

    if (this.noEmptyFields()) {
      this.setState({ stepDisabled: true });
      await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, true);
      if (submissionType === 'none' || submissionType === 'wait') {
        const { currentCase, currentStepIndex } = this.props;
        const nextStep = currentCase.steps[currentStepIndex + 1];

        await caseStore.fetchCases({ silent: true });
      
        this.props.history.push(`/case/${caseId}/${nextStep.name}/${nextStep._id}`);
      } else {
        caseStore.fetchCases({ silent: true });
        this.setState({ showFinishedModal: true });
      }
    } else {
      await CECSimApi.createSubmission(caseId, stepId, inputData, popupHasShown, false);
    }
  }

  noEmptyFields = () => {
    const { sections, inputs, inputData } = this.state;
    
    const inputLength = inputs.length > 0 ? inputs.length : sections.reduce((x, y) => x + y.inputs.length, 0);
    const emptyFields = (Object.keys(inputData).length !== inputLength) || Object.values(inputData).includes('');
    
    this.setState({ emptyFields: emptyFields });
    return !emptyFields;
  }

  onInputChange = (event) => {
    this.setState({
      inputData: {
        ...this.state.inputData,
        [event.target.name]: event.target.value
      }
    });
  }

  onPopupTriggered = (popup) => {
    if (!this.state.popupHasShown) {
      this.setState({ 
        showPopupModal: true, 
        popupHasShown: true, 
        popup: popup
      });
    }
  }
  
  handlePopupClose = () => {
    this.setState({ 
      showPopupModal: false, 
      popup: null
    });
  }

  onFinishedModalClosed = async () => {
    this.setState({ showFinishedModal: false, });

    this.props.history.push('/cases');
  }

  render() {
    const { classes, currentCase, currentStepIndex } = this.props;
    const { sections, inputs, summary, files, inputData, prevInputData, stepDisabled, showFinishedModal, showPopupModal, emptyFields, popup, submissionType, isLoading } = this.state;

    return (!isLoading ?
      <Grid container>
        {/* Print Icon */}
        <Grid item className={classes.printIconGrid}>
          <ReactToPrint 
            trigger={() => 
              <IconButton className={classes.iconButton}>
                <PrintOutlined className={classes.icon} />
              </IconButton>
            }
            content={() => this.componentRef}
          />
        </Grid>

        {/* Print Component */}
        <Grid container item className={classes.printableWrapper}>
          <Grid container item className={classes.printableContainer} ref={el => (this.componentRef = el)}>
            <Header homeType={'learner'}/>
            <Grid item xs={12}>
              <Typography variant="h3">{currentCase.title}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant='h5' className={classes.stepHeader}>Step {currentStepIndex+1}: {currentCase.steps[currentStepIndex].title}</Typography>
            </Grid>
            <Grid container item xs={12} className={classes.submissionContainer}>
              {sections.length === 0 && inputs.map((input, index) => 
                <Grid container item xs={12} className={classes.submissionItem} key={index}>
                  <Grid item xs={12}>
                    <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                  </Grid>
                  <Grid item xs={12} className={classes.indentOnPrint}>
                    <Typography variant="body2" className={classes.response}>{inputData[input.name]}</Typography>
                    {Object.keys(prevInputData).length !== 0 && inputData[input.name] !== prevInputData[input.name] &&
                      <Grid container item xs={12}>
                        <Grid item xs={12}>
                          <Typography variant="body2" className={classes.prevTextLabel}>
                            Previous Response
                          </Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography variant="body2" className={classes.response}>
                            {prevInputData[input.name]}
                          </Typography>
                        </Grid>
                      </Grid>
                    }
                  </Grid>
                </Grid>
              )}

              {sections.length !== 0 && sections.map((section, index) => 
                <Grid container item xs={12} key={index} className={classes.submissionItem}>
                  <Grid item xs={12}>
                    <Typography variant='body1' className={classes.sectionHeader}>{section.sectionName}</Typography>
                  </Grid>
                  {section.inputs.map((input, inputIndex) =>
                    <Grid container item xs={12} className={classes.submissionItem} key={inputIndex}>
                      <Grid item xs={12}>
                        <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                      </Grid>
                      <Grid item xs={12} className={classes.indentOnPrint}>
                        <Typography variant="body2" className={classes.response}>{inputData[input.name]}</Typography>
                        {Object.keys(prevInputData).length !== 0 && inputData[input.name] !== prevInputData[input.name] &&
                          <Grid container item xs={12}>
                            <Grid item xs={12}>
                              <Typography variant="body2" className={classes.prevTextLabel}>
                                Previous Response
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography variant="body2" className={classes.response}>
                                {prevInputData[input.name]}
                              </Typography>
                            </Grid>
                          </Grid>
                        }
                      </Grid>
                    </Grid>
                  )}
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>

        <IntroResources 
          descriptions={this.state.descriptions}
          notes={this.state.notes}
          bullets={this.state.bullets}
          caseNum={currentCase.order}
          stepNum={currentStepIndex+1}
          stepName={currentCase.steps[currentStepIndex].title}
          files={files}
        />
        <HorizontalRule />
        <Grid item xs={12}>
          <Typography variant='h5'>
            {this.state.instruction}
          </Typography>
        </Grid>
        <Grid container>
          {/* Analyze, Decide */}
          {inputs.length > 0 && inputs.map(input =>
            <Input
              key={input.name}
              input={input}
              value={inputData[input.name]}
              prevValue={prevInputData[input.name]}
              disabled={stepDisabled || submissionType === 'none'}
              onInputChange={this.onInputChange}
              onPopupTriggered={this.onPopupTriggered}
            />
          )}

          {/* Prepare */}
          {sections.length > 0 && sections.map((section, index) => (
            <Grid container item xs={12} key={index}>
              <Grid item xs={12} className={classes.sectionHeader}>
                <Typography variant='h3'>{section.sectionName}</Typography>
              </Grid>
              <Grid container>
                {section.inputs.map(input =>
                  <Input
                    key={input.name}
                    input={input}
                    value={inputData[input.name]}
                    prevValue={prevInputData[input.name]}
                    disabled={stepDisabled || submissionType === 'none'}
                    onInputChange={this.onInputChange}
                    onPopupTriggered={this.onPopupTriggered}
                  />
                )}
              </Grid>
            </Grid>
          ))}
        </Grid>
        <Grid container className={classes.buttonGrid}>
          <Grid item xs={8}>
            {summary &&
              <Typography variant='body1' className={classes.summary}>
                {summary}
              </Typography>
            }
          </Grid>
          <Grid container item xs={4}>
            <Grid container justify="flex-end">
              <Grid item xs={6}>
                {submissionType !== 'none' &&
                  <Button disabled={stepDisabled} className={classes.button} onClick={this.onSave}>
                    Save
                  </Button>
                }
                <Button disabled={stepDisabled} className={classNames(classes.button, classes.orangeButton)} onClick={this.onSaveAndSubmit}>
                  {(submissionType === 'none' || submissionType === 'wait') ? 'Continue' : 'Complete'}
                </Button>
                {emptyFields &&
                  <Typography variant='body1' className={classes.error}>
                    You must enter a response in all boxes.
                  </Typography>
                }
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {showPopupModal &&
          <PopupModal
            handleClose={this.handlePopupClose}
            open={showPopupModal}
            popupHeader={popup.header}
            popupBody={popup.body}
            popupInstructions={popup.instructions}
          />
        }
        {showFinishedModal &&
          <FinishedModal
            handleClose={this.onFinishedModalClosed}
            currentCase={currentCase}
            currentStepIndex={currentStepIndex}
            open={showFinishedModal}
          />
        }
      </Grid>
    : 
      <Loading />
    );
  }
}

export default withRouter(withStyles(styles)(inject('caseStore')(observer(Respond))));