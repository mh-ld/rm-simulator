import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Button,
  Grid,
  Dialog,
  IconButton,
} from '@material-ui/core';

import { CloseOutlined } from '@material-ui/icons';

const styles = theme => ({
  dialogPaper: {
    backgroundColor: '#434343',
    borderRadius: '8px',
    boxShadow: 'none',
    overflowX: 'hidden',
    maxWidth: '75vw'
  },
  dialogContent: {
    padding: '1rem 1rem',
  },
  iconGrid: {
    textAlign: 'right',
    width: '100%',
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#FFF',
  },
  dialogBody: {
    paddingTop: '.5rem',
    paddingBottom: '2rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
  },
  headingText: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '24px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '29px',
    marginBottom: '1rem',
  },
  description: {
    color: '#FFF',
    fontFamily: 'Calibri',
    fontSize: '18px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '23px',
    marginBottom: '1rem',
  },
  instructions: {
    color: '#FFF',
    fontFamily: 'Calibri',
    fontSize: '18px',
    fontStyle: 'italic',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '23px',
    marginBottom: '2rem',
    marginTop: '1rem',
  },
  centerOnGrid: {
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#CE722F',
    color: '#FFFFFF',
    fontFamily: 'Calibri',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  }
});

class PopupModal extends Component {

  render() {
    const { classes, handleClose, open, popupHeader, popupBody, popupInstructions } = this.props;

    return (
      <Dialog
        open={open}
        classes={{ paper: classes.dialogPaper }}
        maxWidth={false}
      >
        {
          open ?
            <Grid container justify="center" className={classes.dialogContent}>
              <Grid item className={classes.iconGrid}>
                <IconButton onClick={handleClose} className={classes.iconButton}>
                  <CloseOutlined className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid container justify="center" className={classes.dialogBody}>
                <Grid item container xs={12} direction="row">
                  <Grid item xs={12}>
                    <Typography className={classes.headingText} align="center">
                      {popupHeader}
                    </Typography>
                    {popupBody.map((b, index) => 
                      <Typography key={index} className={classes.description} align="center">
                        {b}
                      </Typography>
                    )}
                    <Typography className={classes.instructions} align="center">
                      {popupInstructions}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} className={classes.centerOnGrid}>
                    <Button onClick={handleClose} className={classes.button}>
                      Close
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid> : <div />
        }
      </Dialog>
    );
  }

}

export default withStyles(styles)(PopupModal);