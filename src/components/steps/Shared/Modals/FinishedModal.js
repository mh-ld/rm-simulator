import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
//import { Link } from 'react-router-dom';
import CompletedIcon from '../Icons/CompletedIcon';
import {
  Typography,
  Grid,
  Dialog,
  IconButton,
} from '@material-ui/core';
import classNames from 'classnames';

import { CloseOutlined } from '@material-ui/icons';

const styles = theme => ({
  dialogPaper: {
    backgroundColor: '#434343',
    borderRadius: '8px',
    boxShadow: 'none',
    overflowX: 'hidden'
  },
  dialogContent: {
    padding: '1rem 1rem',
  },
  closeIconGrid: {
    textAlign: 'right',
    width: '100%',
  },
  iconButton: {
    padding: '0px'
  },
  closeIcon: {
    color: '#FFF',
  },
  completedIconGrid: {
    marginBottom: '1.5rem',
  },
  completedIcon: {
    
  },
  dialogBody: {
    paddingTop: '1rem',
    paddingBottom: '2rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
  },
  headingText: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '25px',
    letterSpacing: 0,
    lineHeight: '30px',
    marginBottom: '1rem',
  },
  description: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: 0,
    lineHeight: '18px',
  },
  instructions: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: 0,
    lineHeight: '18px',
    marginTop: '1rem'
  },
  centerOnGrid: {
    textAlign: 'center'
  },
  goHomeBtnGrid: {
    marginTop: '2rem'
  }
});

class FinishedModal extends Component {
  render() {
    const { classes, finishedModalData, currentStepIndex, currentCase, handleClose } = this.props;

    let finishedText = ''
    switch (currentCase.steps[currentStepIndex].name) {
      case 'analyze':
        finishedText = 'Your analysis findings have been sent to your facilitator.';
        break;
      case 'decide':
        finishedText = 'Your strategy recommendations have been sent to your facilitator.';
        break;
      case 'prepare':
        finishedText = 'Your plan to present, implement and critique your recommended strategy(s) for step ' + (currentStepIndex + 1) + ' have been sent to your facilitator.';
        break;
      case 'record':
        finishedText = 'Your recorded sales strategy meeting presentation has been sent to your facilitator.';
        break;
      default:
        finishedText = 'Your response has been sent to your facilitator.';
    }

    return (
      <Dialog
        open={this.props.open}
        classes={{ paper: classes.dialogPaper }}
        maxWidth={false}
      >
        {
          this.props.open && currentCase && currentCase.steps ?
            <Grid container justify="center" className={classes.dialogContent}>
              <Grid item className={classes.closeIconGrid}>
                <IconButton onClick={handleClose} className={classes.iconButton}>
                  <CloseOutlined className={classes.closeIcon} />
                </IconButton>
              </Grid>
              <Grid container direction="row" className={classes.dialogBody}>
                <Grid item container xs={12} direction="row">
                  <Grid item className={classNames(classes.centerOnGrid, classes.completedIconGrid)} xs={12}>
                    <CompletedIcon className={classes.completedIcon}/>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography className={classes.headingText} align="center">
                      {`Step ${currentStepIndex + 1}:`}
                    </Typography>
                    <Typography className={classes.description} align="center">
                      {finishedText}
                    </Typography>
                    <Typography className={classes.instructions} align="center">
                      {finishedModalData ? finishedModalData.caption : "Upon receiving their feedback take the appropriate actions."}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid> : <div />
        }
      </Dialog>
    );
  }

}

export default withStyles(styles)(FinishedModal);