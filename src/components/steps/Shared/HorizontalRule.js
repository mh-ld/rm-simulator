import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { Grid } from '@material-ui/core';

const styles = theme => ({
  hrGrid: {
    marginTop: '2rem',
    marginBottom: '2rem'
  },
});

class HorizontalRule extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Grid className={classes.hrGrid} item xs={12}>
        <hr style={{ 
          height: '1px', 
          backgroundColor: '#CE722F', 
          border: 'none' 
        }}/>
      </Grid>
    );
  }
}

export default withStyles(styles)(HorizontalRule);