import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography
} from '@material-ui/core';

import Descriptions from './Descriptions';
import Files from './Files';

const styles = theme => ({
  stepTitle: {
    marginBottom: '1rem'
  },
  sideNote: {
    marginBottom: '1rem',
    fontStyle: 'italic',
    color: '#000',
    fontFamily: 'Calibri',
    fontSize: '16px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '16px',
    textAlign: 'left'
  },
  stepInstructions: {
    marginBottom: '2rem'
  },
});

class IntroResources extends Component {
  render() {
    const { classes, descriptions, notes, bullets, stepNum, stepName, files } = this.props;

    return (
      <Grid item>
        <Typography variant='h3' className={classes.stepTitle}>
          Step {stepNum}: {stepName}
        </Typography>
        <Descriptions 
          descriptions={descriptions}
          numbered={stepNum === 4}
        />
        {notes && notes.length > 0 && 
          <Grid>
            {notes.map((text, index) => 
              <Typography variant='body2' className={classes.sideNote} key={index}>
                {text}
              </Typography>
            )}
            {bullets && bullets.length > 0 && 
              <ul>
                {bullets.map((text, index) => 
                  <li variant='body2' className={classes.sideNote} key={index}>
                    {text}
                  </li>
                )}
              </ul>
            }
          </Grid>
        }
        {files && 
          <Grid>
            {files.length > 1 && 
              <Grid item xs={12}>
                <Typography variant='h3' className={classes.stepInstructions}>
                  Use your advanced skills to determine the appropriate resources.
                </Typography>
              </Grid>
            }
            {stepNum === 4 &&
              <Grid item xs={12}>
                <Typography variant='h3'>
                  PowerPoint Template
                </Typography>
              </Grid>
            }
            <Files files={files} />
            {stepNum === 1 &&
              <Grid item xs={12} style={{marginTop: '1rem'}}>
                <Typography variant='body2' className={classes.sideNote}>
                  If you would like to view different months in the Revenue Planning Template, you must download the tool to your PC.
                </Typography>
              </Grid>
            }
          </Grid>
        }
      </Grid>
    );
  }
}

export default withStyles(styles)(IntroResources);