import React from 'react';

const CompletedIcon = () => (

  <svg width="62px" height="62px" viewBox="0 0 62 62" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Step-1-completed" transform="translate(-540.000000, -193.000000)">
        <g id="Group-3" transform="translate(540.000000, 193.000000)">
          <circle id="Oval" fill="#CE722F" cx="31" cy="31" r="31"></circle>
          <polygon id="Path" fill="#FFFFFF" fillRule="nonzero" points="26.59 34.610608 19.54 27.221216 16 30.915912 23.05 38.305304 26.59 42 30.13 38.305304 46 21.694696 42.46 18"></polygon>
        </g>
      </g>
    </g>
  </svg>
);

export default CompletedIcon;