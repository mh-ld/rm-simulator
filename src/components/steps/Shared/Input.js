import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography,
  TextField,
  Link
} from '@material-ui/core';

const styles = theme => ({
  textField: {
    width: '100%'
  },
  inputGrid: {
    marginTop: '2rem'
  },
  textLabel: {
    marginBottom: '.5rem'
  },
  tip: {
    marginBottom: '.5rem',
    fontStyle: 'italic'
  },
  link: {
    display: 'block',
    color: '#243961',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '12px',
    lineHeight: '14px',
    textDecoration: 'underline'
  },
  prevResponseGrid: {
    marginTop: '1rem'
  }
});

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };
  }

  onTogglePrevious = () => {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  render() {
    const { classes, input = {}, value, prevValue, disabled, onInputChange, onPopupTriggered } = this.props;
    const { expanded } = this.state;

    const differentPrevValue = prevValue && value !== prevValue;

    return (
      <Grid container item xs={12} className={classes.inputGrid}>
        <Grid container item direction='row' justify='space-between' className={classes.textLabel}>
          <Grid item>
            <Typography variant="body2">{input.text}</Typography>
            {input.tip && <Typography variant="body2" className={classes.tip}>{input.tip}</Typography>}
          </Grid>
          {differentPrevValue &&
            <Grid item>
              <Link onClick={this.onTogglePrevious} className={classes.link}>
                {expanded ? 'Hide previous response' : 'View previous response'}
              </Link>
            </Grid>
          }
        </Grid>
        <Grid item xs={12}>
          <TextField
            onChange={onInputChange}
            onFocus={input.popup ? () => onPopupTriggered(input.popup) : null}
            name={input.name}
            value={value}
            multiline
            className={classes.textField}
            variant="outlined"
            margin='none'
            disabled={disabled}
          />
        </Grid>
        {differentPrevValue && expanded && 
          <Grid item xs={12} className={classes.prevResponseGrid}>
            <Grid item>
              <Typography variant="body2" className={classes.textLabel}>Your previous response</Typography>
            </Grid>
            <Grid item>
              <TextField
                name={input.name}
                value={prevValue}
                multiline
                className={classes.textField}
                variant="outlined"
                margin='none'
                disabled={true}
              />
            </Grid>
          </Grid>
        }
      </Grid>
    );
  }
}

export default withStyles(styles)(Input);