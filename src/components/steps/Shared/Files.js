import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography,
  IconButton
} from '@material-ui/core';

import {
  FolderOpenOutlined,
  DescriptionOutlined,
  SubdirectoryArrowLeftOutlined
} from '@material-ui/icons';


const styles = theme => ({
  icon: {
    color: '#243961',
    fontSize: '1.5rem'
  },
  iconGrid: {
    margin: '.75rem',
    textAlign: 'center',
    width: '85px'
  },
  fileName: {
    wordWrap: 'break-word'
  }
});

class Files extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentFolder: []
    }

  }

  onFileOrFolderClick = (file) => {
    const { currentFolder } = this.state;

    if (file.type === 'folder') {
      this.setState({ currentFolder: [...currentFolder, file.name] });
    } else {
      window.open(file.url)
    }
  }

  onBackClick = () => {
    const { currentFolder } = this.state;
    let newCurrentFolder = [...currentFolder];
    newCurrentFolder.pop();

    this.setState({ currentFolder: newCurrentFolder});
  }

  render() {
    const { classes, files } = this.props;
    const { currentFolder } = this.state;
    const isSubdirectory = currentFolder.length > 0;
    let currentFiles = [...files];

    currentFolder.forEach(dirName => {
      const dirObject = currentFiles.find(cf => cf.name === dirName);
      currentFiles = dirObject.files;
    });

    const sortedFiles = [...currentFiles];

    sortedFiles.sort((a, b) => {
      if (a.type === 'folder' && b.type !== 'folder') return -1;
      if (b.type === 'folder' && a.type !== 'folder') return 1;
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    return (
      <Grid container className={classes.rootGrid}>
        {
          isSubdirectory ?
            <Grid item className={classes.iconGrid} onClick={this.onBackClick}>
              <IconButton><SubdirectoryArrowLeftOutlined className={classes.icon}/></IconButton>
              <Typography align="center" variant='h4' className={classes.fileName}>Back</Typography>
            </Grid>
            : null
        }
        {sortedFiles.map((file, index) =>
          <Grid key={index} item className={classes.iconGrid} onClick={this.onFileOrFolderClick.bind(this, file)}>
            <IconButton>
              {
                file.type === 'folder' ?
                  <FolderOpenOutlined className={classes.icon} /> 
                  : <DescriptionOutlined className={classes.icon} />
              }
            </IconButton>
            <Typography align="center" variant='h4' className={classes.fileName}>
              {file.name}
            </Typography>
          </Grid>
        )}
      </Grid>
    );
  }
}

export default withStyles(styles)(Files);