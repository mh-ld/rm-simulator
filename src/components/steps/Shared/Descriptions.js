import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography,
  Link
} from '@material-ui/core';

const styles = theme => ({
  paragraphContainer: {
    marginBottom: '1rem'
  },
  numberedParagraphContainer: {
    marginBottom: '2rem'
  },
  number: {
    marginRight: '1.5rem',
    fontSize: '36px',
    fontFamily: 'Calibri'
  },
  paragraph: {
    whiteSpace: 'pre-wrap',
    lineHeight: '24px'
  }
});

class Descriptions extends Component {
  render() {
    const { classes, descriptions, numbered=false } = this.props;

    return (
      <Grid container>
        {descriptions.map((description, index) => {
          let position = description.indexOf('click here');

          const paragraphElement = (position < 0) ? 
            (
              <>{description}</>
            ) : (
              <>
                {description.substring(0, position+6)}
                <Link
                  href='https://support.microsoft.com/en-us/office/record-a-meeting-in-teams-34dfbe7f-b07d-4a27-b4c6-de62f1348c24'
                  underline='always'
                  target='_blank'
                  rel='noopener noreferrer'
                >here</Link>
                {description.substring(position+10)}
              </>
            );

          return (
            <Grid item xs={12} key={index}>
              {(index === 0 || !numbered) ?
                <Grid item className={classes.paragraphContainer}>
                  <Typography variant="body1" className={classes.paragraph}>
                    {paragraphElement}
                  </Typography>
                </Grid>
              :
                <Grid item className={classes.numberedParagraphContainer} style={{ display: "flex", flexDirection: "row" }}>
                  <Grid item>
                    <Typography variant="h1" className={classes.number}>
                      {index}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body1" className={classes.paragraph}>
                      {paragraphElement}
                    </Typography>
                  </Grid>
                </Grid>
              }
            </Grid>
          )
        })}
      </Grid>
    );
  }
}

export default withStyles(styles)(Descriptions);