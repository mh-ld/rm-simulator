import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Typography,
  Grid,
  Paper,
  Link
} from '@material-ui/core';

import CaseBox from './CaseBox';

const styles = theme => ({
  rootPaper: {
    minHeight: 'calc(100vh - 64px)',
  },
  rootGrid: {
    paddingTop: theme.spacing.unit * 8,
  },
  titleGrid: {
    marginBottom: theme.spacing.unit * 2,
  },
  descriptionGrid: {
    marginBottom: theme.spacing.unit * 2,
  },
  description: {
    marginBottom: theme.spacing.unit * 2,
  },
  caseBoxGrid: {
    marginBottom: theme.spacing.unit,
  },
  caseBox: {
    marginBottom: theme.spacing.unit * 3,
  },
  feedbackLink: {
    color: '#243961',
    fontFamily: 'Calibri',
    fontSize: '16px',
    fontWeight: 'bold',
    letterSpacing: '0',
    lineHeight: '18px',
    marginBottom: theme.spacing.unit * 10,
    textDecoration: 'underline'
  }
});

class Main extends Component {
  render() {
    const { classes, cases } = this.props;
    let prevCaseCompleted = true;

    const descriptions = [
      "As you progress through the Revenue Management Advanced Strategy + Influence program, you will have the opportunity to practice applying the advanced revenue management and leadership skills you learn about in case study activities. The three case study activities feature three sample hotel scenarios in three different markets globally.",
      "These case study activities are self-paced. After you complete training on the advanced leadership skills for a specific week, you will practice applying those skills in a case study activity. You will complete the four steps that make up case study 1 over the course of several weeks. Case studies 2 and 3 will be completed (all steps) in the weeks they are assigned.",
      "When you first access each case study activity, the screen for step 1 will display. Begin by reviewing the case study overview and information provided. In step 1, you will see a section with folders containing reports, tools, and Excel workbooks with information specific to the scenario hotel. The folders containing these resources are posted in no particular order. Just like in the real world, you will have several resources available. You may or may not need to analyze all of the data on all of the resources provided. In other words, once you have identified the issue and conducted preliminary analysis, you should use your skills to identify the resources that will provide you with the information you need to conduct a deeper analysis and not spend time looking at reports and tools that are less valuable.",
      "At each step, you will see fields where you will document your progress through the case study. After entering your information in these fields, click on the SAVE button and then click on the COMPLETE button to send the information to your facilitator for review. Once your facilitator has reviewed the information, you will receive an email with a link to their feedback. Review their feedback and take the appropriate actions. The feedback may be a note letting you know you have successfully completed the step(s) or instructing you to reevaluate some additional information, make changes, and resubmit.",
      "When you are ready, click on the START/RESUME button to access a case study activity."
    ];
    
    return (
      <Paper className={classes.rootPaper}>
        <Grid
          container
          alignItems="center"
          alignContent="center"
          justify="center"
          direction="row"
          className={classes.rootGrid}
        >
          <Grid container item xs={8} direction="row">
            <Grid container item direction="row" xs={12}>
              <Grid item className={classes.titleGrid} xs={10}>
                <Typography variant="h1">
                  Revenue Management Advanced Strategy + Influence
                </Typography>
              </Grid>
              <Grid item className={classes.descriptionGrid}>
                <Typography variant="h2" className={classes.description}>
                  Case Study Activities
                </Typography>
                {descriptions.map((text, index) =>
                  <Typography variant="body1" key={index} className={classes.description}>
                    {text}
                  </Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
          <Grid container item xs={8} className={classes.caseBoxGrid} justify="center">
            {cases.length > 0 && cases.map((caseData, index) => {
                let caseBox = (
                  <Grid key={index} container item className={classes.caseBox}>
                    <CaseBox 
                      caseNumber={index+1}
                      prevCaseCompleted={prevCaseCompleted}
                      {...caseData}
                    />
                  </Grid>
                );
                prevCaseCompleted = caseData.stepsCompleted === caseData.steps.length;
                return (caseBox);
            })}
          </Grid>
          <Grid container item xs={8} className={classes.feedbackLink}>
            <Grid container item>
              <Link
                color="inherit"
                component={RouterLink}
                to={'/submissions'}
              >
                Review Your Facilitor's Feedback
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withStyles(styles)(observer(Main));