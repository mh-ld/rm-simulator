import React from 'react';
import { observer } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import {
  Typography,
  Grid,
  Paper,
  Button
} from '@material-ui/core';

const styles = theme => ({
  casebox: {
    padding: '1rem',
    width: '100%',
    border: '1px solid #BFBECD',
    borderRadius: '4px'
  },
  caseboxHighlighted: {
    padding: '1rem',
    width: '100%',
    border: '1px solid #CE722F',
    borderRadius: '4px'
  },
  caseNumber: {
    textAlign: 'center'
  },
  caseTitle: {
    marginBottom: '.5rem'
  },
  button: {
    width: '100%'
  }
});

const goToCasePart = (props) => {
  let nextStep = props.steps.find((step) => step.completed === false);

  nextStep = nextStep ? nextStep : props.steps[0];

  props.history.push(`/case/${props._id}/${nextStep.name}/${nextStep._id}`);
}

const CaseBox = (props) => {
  const { classes, prevCaseCompleted } = props;
  let buttonText, caseBoxStyle;

  if (props.stepsCompleted === props.steps.length) {
    buttonText = 'Completed';
    caseBoxStyle = classes.casebox;
  } else if (props.startedCase || ((props.stepsCompleted > 0) && (props.stepsCompleted < props.steps.length))) {
    buttonText = 'Resume';
    caseBoxStyle = classes.caseboxHighlighted;
  } else {
    buttonText = 'Start';
    caseBoxStyle = prevCaseCompleted ? classes.caseboxHighlighted : classes.casebox;
  }    

  return (
    <Grid container item xs={12}>
      <Paper elevation={0} className={caseBoxStyle}>
        <Grid container direction="row" justify="center" alignItems="center" spacing={24}>
          <Grid item xs={1}>
            <Typography variant="h1" className={classes.caseNumber}>{props.caseNumber}</Typography>
          </Grid>
          <Grid item xs={7}>
            <Typography variant="h2" className={classes.caseTitle}>{props.title}</Typography>
            <Typography variant="body2">{props.caseLabel}</Typography>
          </Grid>
          <Grid container item xs={4} justify="center">
            <Grid item xs={6}>
              <Button 
                onClick={goToCasePart.bind(this, props)} 
                className={classes.button} 
                disabled={!prevCaseCompleted}
              >
                {buttonText}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  )
};

export default withRouter(withStyles(styles)(observer(CaseBox)));