import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import ReactToPrint from 'react-to-print';
import classNames from 'classnames';

import {
  Grid,
  Typography,
  Button,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel,
  Tooltip,
  Link,
  IconButton
} from '@material-ui/core';
import { 
  HelpOutlineOutlined, 
  ExpandLessRounded, 
  ExpandMoreRounded, 
  PrintOutlined
} from '@material-ui/icons';

import Header from '../Header/Header';
import IntroResources from '../../steps/Shared/IntroResources';
import Input from '../../steps/Shared/Input';
import Loading from '../Loading'

import CECSimApi from '../../../api';
import { apiURL } from '../../../config';

const styles = theme => ({
  videoContainer: {
    marginTop: '1rem',
    marginBottom: '1rem'

  },
  instructions: {
    marginBottom: '2rem'
  },
  stepHeader: {
    textTransform: 'uppercase',
    marginBottom: '2rem'
  },
  submissionItem: {
    marginTop: '1rem'
  },
  sectionHeader: {
    fontStyle: 'italic',
    fontWeight: '700'
  },
  question: {
    fontStyle: 'italic'
  },
  response: {
    marginTop: '.5rem',
    color: '#434343',
    whiteSpace: 'pre-wrap'
  },
  textField: {
    width: '100%'
  },
  textLabel: {
    marginBottom: '-1rem',
    marginTop: '2rem',
  },
  outlinedInput: {
    '&$focused': {
      color: '#434343',
    },
    '&$focused $notchedOutline': {
      borderColor: '#CE722F',
      borderWidth: '1px',
    }
  },
  focused: {},
  notchedOutline: {},
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    marginBottom: '1rem',
    width: '100%',
  },
  orangeButton: {
    backgroundColor: '#CE722F',
  },
  checkboxGrid: {
    marginTop: '1rem'
  },
  radioContainer: {
    background: 'none'
  },
  radioGroupLabel: {
    color: '#243961',
    fontSize: '15px'
  },
  dropdownContainer: {
    background: '#fff',
    borderRadius: '5px',
    marginBottom: '2rem'
  },
  dropdownHeader: {
    padding: '8px',
    cursor: 'pointer'
  },
  dropdownContent: {
    padding: '8px',
    marginTop: '1rem'
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#243961'
  },
  printGrid: {
    marginTop: '-50px'
  },
  showOnPrint: {
    display: 'none'
  },
  hideOnPrint: {
    display: 'block'
  },
  '@media print': {
    printableContainer: {
      marginTop: '64px',
      padding: '20px'
    },
    showOnPrint: {
      display: 'block'
    },
    hideOnPrint: {
      display: 'none'
    },
    indentOnPrint: {
      paddingLeft: '12px'
    },
    stepHeader: {
      marginTop: '1rem',
      marginBottom: '1rem'
    },
    submissionContainer: {
      marginTop: '-.5rem',
    },
    textLabel: {
      marginBottom: '0px'
    },
    prevTextLabel: {
      fontStyle: 'italic',
      fontWeight: 300,
      marginTop: '1rem'
    }
  }
});

class ClassSubmissions extends Component {

  constructor(props) {
    super(props);

    this.state = {
      stepData: null,
      submission: null,
      prevSubmission: null,
      response: '',
      prevResponse: null,
      recordingUrls: [],
      markDone: null,
      feedbackDisabled: true,
      showDropdown: false,
      groupStepDatas: null,
      groupIndex: 0,
      submissionType: 'normal'
    };
  }

  async componentDidMount() {
    const { data: stepData } = await CECSimApi.getStepData(this.props.match.params.stepDataId);

    let numSubs = stepData.submissions.length;
    let lastSub = stepData.submissions[numSubs-1];

    const newState = {
      stepData: stepData,
      submission: lastSub,
      prevSubmission: numSubs > 1 ? stepData.submissions[numSubs-2] : null,
      response: (lastSub.feedback && lastSub.feedback.response) || '',
      prevResponse: numSubs > 1 ? stepData.submissions[numSubs-2].feedback.response : null,
      recordingUrls: (lastSub.data.compositionIds || []).map(fileName => `${apiURL}/rm/recording/${fileName}`),
      markDone: stepData.status === 'Complete' ? 'yes' : null,
      feedbackDisabled: stepData.status !== 'Needs Review'
    };

    const caseStepData = stepData.case.stepData.find(step => step.stepId === stepData.step._id).data;
    newState.submissionType = caseStepData.submissionType;

    if (caseStepData.submissionType === 'group') {
      const groupPromises = caseStepData.groupStepIds.map(async stepId => {
        const { data } = await CECSimApi.getUserStepData(stepData.learnerEid, stepData.case._id, stepId);
        return data;
      });

      newState.groupStepDatas = await Promise.all(groupPromises);
      newState.groupStepDatas.push(stepData);

      const firstStepData = newState.groupStepDatas[0];
      numSubs = firstStepData.submissions.length;
      lastSub = firstStepData.submissions[numSubs-1];

      newState.groupIndex = 0;
      newState.stepData = firstStepData;
      newState.submission = lastSub;
      newState.prevSubmission = numSubs > 1 ? firstStepData.submissions[numSubs-2] : null;
      newState.recordingUrls = (lastSub.data.compositionIds || []).map(fileName => `${apiURL}/rm/recording/${fileName}`);
    }

    this.setState({ ...newState });
  }

  toggleDropdown = () => {
    const { showDropdown } = this.state;
    this.setState({ showDropdown: !showDropdown });
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  }

  onDownload(fileName) {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
        const blobUrl = window.URL.createObjectURL(xmlHttp.response);
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = fileName;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    };
    xmlHttp.responseType = 'blob';
    xmlHttp.open('GET', `${apiURL}/rm/recording/${fileName}`, true);
    xmlHttp.send(null);
  }

  changeStep = (newIndex) => {
    const { groupStepDatas } = this.state;

    const newStepData = groupStepDatas[newIndex];

    const numSubs = newStepData.submissions.length;
    const lastSub = newStepData.submissions[numSubs-1];

    this.setState({
      groupIndex: newIndex,
      stepData: newStepData,
      submission: lastSub,
      prevSubmission: numSubs > 1 ? newStepData.submissions[numSubs-2] : null,
      recordingUrls: (lastSub.data.compositionIds || []).map(fileName => `${apiURL}/rm/recording/${fileName}`)
    });
  }

  onPrevStep = () => {
    const { groupIndex } = this.state;
    this.changeStep(groupIndex > 0 ? groupIndex - 1 : groupIndex);
  }

  onNextStep = () => {
    const { groupIndex, groupStepDatas } = this.state;
    this.changeStep(groupIndex < groupStepDatas.length - 1 ? groupIndex + 1 : groupIndex);
  }

  onSave = async () => {
    const { stepDataId } = this.props.match.params;
    const { response, markDone } = this.state;

    await CECSimApi.giveFeedback(stepDataId, response, markDone === 'yes');

    this.props.history.push(`/class/${this.props.classId}/submissions`);
  }

  render() {
    const { classes } = this.props;
    const { stepData, submission, prevSubmission, response, recordingUrls, prevResponse, markDone, feedbackDisabled, showDropdown, submissionType, groupStepDatas, groupIndex } = this.state;
    let inputData, inputs, sections, recordingRounds, savedResponses, prevSavedResponses, finalStepData;
    
    if (submission) {
      inputData = stepData.case.stepData[stepData.step.order - 1].data;
      sections = inputData.sections || [];
      inputs = inputData.inputs || [];
      recordingRounds = inputData.rounds;
      savedResponses = submission.data;
      if (prevSubmission) {
        prevSavedResponses = prevSubmission.data;
      }
      finalStepData = submissionType !== 'group' || (groupStepDatas && (groupStepDatas.length === groupIndex + 1));
    }

    return (submission ?
      <Grid container className={classes.rootGrid}>
        <Grid container item xs={12}>
          {/* Print Icon */}
          <Grid item className={classes.printGrid}>
            <ReactToPrint 
              trigger={() => 
                <IconButton className={classes.iconButton}>
                  <PrintOutlined className={classes.icon} />
                </IconButton>
              }
              content={() => this.componentRef}
            />
          </Grid>

          {/* Dropdown */}
          <Grid container item className={classes.dropdownContainer}>
            <Grid container item direction='row' justify='space-between' className={classes.dropdownHeader} onClick={this.toggleDropdown}>
              <Typography variant="h3">View Step Introduction and Resources</Typography>
              <ExpandMoreRounded className={classes.icon} style={{display: showDropdown && 'none'}}/>
              <ExpandLessRounded className={classes.icon} style={{display: !showDropdown && 'none'}}/>
            </Grid>
            <Grid item className={classes.dropdownContent} style={{display: !showDropdown && 'none'}}>
              <IntroResources 
                descriptions={inputData.descriptions}
                notes={inputData.notes}
                bullets={inputData.bullets}
                caseNum={stepData.case.order}
                stepNum={stepData.step.order}
                stepName={stepData.step.title}
                files={inputData.files}
              />
            </Grid>
          </Grid>

          {/* Submission Content */}
          <Grid container item className={classes.printableContainer} ref={el => (this.componentRef = el)}>
            <Header className={classes.showOnPrint} homeType={'facilitator'}/>
            <Grid item xs={12}>
              <Typography variant="h3">{stepData.case.title}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body1" className={classNames(classes.instructions, classes.hideOnPrint)}>Review and comment on learner's submissions below.</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant='h5' className={classes.stepHeader}>Step {stepData.step.order}: {stepData.step.title}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h3" className={classes.hideOnPrint}>{stepData.step.description}</Typography>
              <Typography variant="h3" className={classes.showOnPrint}>Learner Response:</Typography>
            </Grid>
            <Grid container item xs={12} className={classes.submissionContainer}>
              {/* Analyze, Decide */}
              {recordingUrls.length === 0 && sections.length === 0 && inputs.map((input, index) =>
                <Grid container item xs={12} className={classes.submissionItem} key={index}>
                  <Grid item xs={12}>
                    <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                  </Grid>
                  <Grid item xs={12} className={classes.indentOnPrint}>
                    <Typography variant="body2" className={classes.response}>{savedResponses[input.name]}</Typography>
                    {prevSubmission && savedResponses[input.name] !== prevSavedResponses[input.name] &&
                      <Grid container item xs={12} className={classes.showOnPrint}>
                        <Grid item xs={12}>
                          <Typography variant="body2" className={classes.prevTextLabel}>
                            Previous Response
                          </Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography variant="body2" className={classes.response}>
                            {prevSavedResponses[input.name]}
                          </Typography>
                        </Grid>
                      </Grid>
                    }
                  </Grid>
                </Grid>
              )}

              {/* Prepare */}
              {recordingUrls.length === 0 && sections.length !== 0 && sections.map((section, index) => 
                <Grid container item xs={12} key={index} className={classes.submissionItem}>
                  <Grid item xs={12}>
                    <Typography variant='body1' className={classes.sectionHeader}>{section.sectionName}</Typography>
                  </Grid>
                  {section.inputs.map((input, inputIndex) =>
                    <Grid container item xs={12} className={classes.submissionItem} key={inputIndex}>
                      <Grid item xs={12}>
                        <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                      </Grid>
                      <Grid item xs={12} className={classes.indentOnPrint}>
                        <Typography variant="body2" className={classes.response}>{savedResponses[input.name]}</Typography>
                        {prevSubmission && savedResponses[input.name] !== prevSavedResponses[input.name] &&
                          <Grid container item xs={12} className={classes.showOnPrint}>
                            <Grid item xs={12}>
                              <Typography variant="body2" className={classes.prevTextLabel}>
                                Previous Response
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography variant="body2" className={classes.response}>
                                {prevSavedResponses[input.name]}
                              </Typography>
                            </Grid>
                          </Grid>
                        }
                      </Grid>
                    </Grid>
                  )}
                </Grid>
              )}

              {/* Record */}
              {recordingUrls.length > 0 && 
                <Grid container item xs={12} className={classes.submissionItem}>
                  {/* Video Section */}
                  <Grid item xs={12} className={classes.sectionHeader}>
                    <Typography variant='h3'>Video</Typography>
                  </Grid>
                  {recordingUrls.map((recordingUrl, index) => 
                    <Grid container item xs={8} key={index}>
                      <Grid item xs={12}>
                        <Typography variant="body1" className={classes.question}>
                          {recordingRounds[index].text}
                        </Typography>
                      </Grid>
                      <Grid item container xs={12} justify="center" className={classes.videoContainer}>
                        <video controls width="800" height="450" src={recordingUrl} type="video/webm" />
                      </Grid>
                    </Grid>
                  )}
                  {/* PPT Section */}
                  {savedResponses.ppt &&
                    <Grid container item xs={12}>
                      <Grid item xs={12} className={classes.sectionHeader}>
                        <Typography variant='h3'>Powerpoint Presentation</Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Link onClick={this.onDownload.bind(this, savedResponses.ppt.fileName)}>
                          {savedResponses.ppt.name}
                        </Link>
                      </Grid>
                    </Grid>
                  }
                </Grid>
              }
            </Grid>

            {/* Response (Interactive) */}
            <Grid container item xs={12} className={classes.hideOnPrint}>
              <Grid item xs={12}>
                <Typography variant="h3" className={classes.textLabel}>
                  Enter your feedback to the learner's responses:
                </Typography>
              </Grid>
              <Input
                value={response}
                prevValue={prevResponse}
                disabled={feedbackDisabled}
                onInputChange={this.handleChange('response')}
              />
            </Grid>

            {/* Response (Printable) */}
            <Grid container item xs={12} className={classes.showOnPrint}>
              <Grid item xs={12}>
                <Typography variant="h3" className={classes.textLabel}>
                  Facilitator Response:
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="body2" className={classes.response}>
                  {response}
                </Typography>
              </Grid>
              {prevResponse &&
                <Grid container item xs={12} className={classes.indentOnPrint}>
                  <Grid item xs={12}>
                    <Typography variant="body2" className={classes.prevTextLabel}>
                      Previous Response
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="body2" className={classes.response}>
                      {prevResponse}
                    </Typography>
                  </Grid>
                </Grid>
              }
            </Grid>
          </Grid>

          {/* Navigation/Submission */}
          <Grid container item className={classes.buttonGrid}>
            {finalStepData &&
              <Grid container direction='row' justify="flex-end">
                <Grid item className={classes.checkboxGrid}>
                  <FormControl disabled={feedbackDisabled} className={classes.radioContainer}>
                    <FormLabel className={classes.radioGroupLabel}>
                      <span>
                        Mark step as Done? <Tooltip 
                          placement='top'
                          arrow='true'
                          title='Marking as "done" will send feedback and learner cannot make changes to their responses.'
                        >
                          <HelpOutlineOutlined className={classes.radioGroupLabel}/>
                        </Tooltip>
                      </span>
                    </FormLabel>
                    <RadioGroup
                      defaultValue={markDone}
                      onChange={this.handleChange('markDone')}
                      row={true}
                    >
                      <FormControlLabel value='yes' label='Yes' control={<Radio />} />
                      <FormControlLabel value='no' label='No' control={<Radio />} />
                    </RadioGroup>
                  </FormControl>
                </Grid>
              </Grid>
            }
            <Grid container direction='row' justify="space-between">
              <Grid item>
                {groupIndex > 0 &&
                  <Button className={classes.button} onClick={this.onPrevStep} variant='outlined'>
                    Previous
                  </Button>
                }
              </Grid>
              <Grid item>
                {!finalStepData ?
                  <Button 
                    className={classes.button} 
                    onClick={this.onNextStep}
                  >
                    Next
                  </Button>
                :
                  <Button 
                    className={classNames(classes.button, classes.orangeButton)} 
                    onClick={this.onSave} 
                    disabled={feedbackDisabled || !markDone}
                  >
                    Send Feedback
                  </Button>
                }
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    : 
      <Loading />
    );
  }
}

export default withRouter(withStyles(styles)(inject('caseStore')(observer(ClassSubmissions))));