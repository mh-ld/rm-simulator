import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import { Link as RouterLink } from 'react-router-dom';
import _ from 'lodash';
import {
  Typography,
  Grid,
  Paper,
  Button,
} from '@material-ui/core';

const styles = theme => ({
  rootDiv: {
  },
  rootPaper: {
    height: 'calc(100vh - 64px)',
    paddingTop: '0px'
  },
  fullGrid: {
    height: 'calc(100vh - 64px)',
    width: 'calc((100vh - 64px)*1.643399)'
  },
  image: {
    backgroundImage: 'url(https://isistudio-public.s3.amazonaws.com/cec-simulator/revenue-management/Intro/Intro%20Background.jpg)',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    height: '100%',
    width: '0',
    paddingRight: '100%',
    overflow: 'hidden'
  },
  leftNavContainer: {
    height: '100%',
    position: 'relative',
  },
  leftNavBarMask: {
    boxShadow: '2px 0px 4px rgba(0,0,0,0.5)',
    height: '100%',
    left: '-28.8675vh',
    position: 'absolute',
    top: '0px',
    transform: 'skew(-30deg)',
    width: '60vw'
  },
  leftNavBar: {
    backgroundColor: '#355894',
    backgroundImage: 'linear-gradient(90deg, #355894 0%, #243961 100%)',
    borderRight: '6px solid #CE722F',
    height: '100%',
    left: '0px',
    opacity: '.75',
    position: 'absolute',
    width: '60vw',
  },
  leftNavContentWrapper: {
    position: 'absolute',
    width: 'calc(60vw - 28.8675vh)',
  },
  leftNavContent: {
    paddingLeft: theme.spacing.unit * 6,
    paddingTop: theme.spacing.unit * 16,
  },
  welcomeItem: {
    color: '#FFFFFF',
    marginBottom: theme.spacing.unit * 2,
  },
  description: {
    color: '#FFFFFF',
    marginBottom: theme.spacing.unit * 2,
  },
  viewAddClassesGrid: {
    marginBottom: theme.spacing.unit * 1.5,
  },
  boldWhite: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Bold',
    fontSize: '14px',
    letterSpacing: '1px',
    lineHeight: '17px',
  },
  button: {
    marginTop: theme.spacing.unit * 2,
    backgroundColor: '#FFF',
    color: '#243961',
    width: '100%',
    borderRadius: '5px',
    '&:hover': {
      backgroundColor: '#CCC'
    }
  }
});

class Intro extends Component {
  render() {
    const { classes, user, cases, isAdmin, isFacilitator, isParticipant, classClosed, isActive } = this.props;

    let firstName = _.get(user, 'dlz2.profile.userFileRaw2.First Name', '');
    if (firstName === '') {
      firstName = _.get(user, 'name', '').split(' ')[0];
    }

    const descriptions = [
      "Welcome to the Revenue Management Learning Portal! This learning environment will allow you to practice applying the advanced diagnostic, strategic, decision-making, communication, and leadership skills you learn about in three case study activities.",
      "Completing these case study activities will allow you to practice the skills you have learned and give you the opportunity to evaluate your ability to successfully analyze and convert data into optimally positioned strategy recommendations, communicate with confidence the relevant analysis findings and strategy recommendations to various audience groups, and gain consensus among team members. You will also practice putting together action plans for implementing the strategies and critiquing their effectiveness.",
      "Click on the START button to begin."
    ];

    let learnerButtonText;
    if (!cases.some(c => c.stepsCompleted !== c.steps.length)) {
      learnerButtonText = 'REVIEW'
    } else if (cases.some(c => c.startedCase)) {
      learnerButtonText = 'RESUME'
    } else {
      learnerButtonText = 'START'
    }

    return (
      <div className={classes.rootDiv}>
        <Paper className={classes.rootPaper} elevation={0}>
          <Grid container justify="center">
            <Grid container justify="center" className={classes.fullGrid}>
              <Grid container alignContent="center" alignItems="center" className={classes.image}>
                <div className={classes.leftNavContainer}>
                  <div className={classes.leftNavBarMask}>
                    <div className={classes.leftNavBar}>
                    </div>
                  </div>
                  <div className={classes.leftNavContentWrapper}>
                    <Grid container item xs={10} alignContent="center" alignItems="center" className={classes.leftNavContent}>  
                      <Grid item xs={12}>
                        <Typography variant="h2" className={classes.welcomeItem}>
                          Welcome{firstName !== '' && ', ' + firstName}
                        </Typography>
                      </Grid>
                      {isActive ? (
                        <Grid item xs={12}>
                          {descriptions.map((text, index) =>
                            <Typography variant="body1" key={index} className={classes.description}>
                              {text}
                            </Typography>
                          )}
                        </Grid>
                      ) : (
                        <Grid item xs={12}>
                          <Typography variant="body1" className={classes.description}>
                            You are not currently in an active class.
                          </Typography>
                        </Grid>
                      )}
                      {isActive &&
                        <Grid item xs={4}>
                          {isAdmin &&
                            <Grid item className={classes.viewAddClassesGrid}>
                              <Typography className={classes.boldWhite}>VIEW OR ADD CLASSES</Typography>
                            </Grid>
                          }
                          {(isAdmin || isFacilitator) &&
                            <Button 
                              className={classes.button} 
                              component={RouterLink} 
                              to="/classes"
                            >
                              MANAGE CLASSES
                            </Button>
                          }
                          {isParticipant && !classClosed &&
                            <Button 
                              className={classes.button} 
                              component={RouterLink} 
                              to="/cases"
                            >
                              {learnerButtonText}
                            </Button>
                          }
                        </Grid>
                      }
                    </Grid>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}
export default withStyles(styles)(observer(Intro));