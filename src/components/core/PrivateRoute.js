import React from 'react';
import { Route } from 'react-router-dom';
import { rootURL } from '../../config';

const PrivateRoute = ({ isAuthenticated = false, isActive = false, ssoUrl, component: Component, ...rest }) => {
  const goToSsoUrl = () => {
    window.location = ssoUrl;
  };

  const goHome = () => {
    window.location = window.location.origin + rootURL;
  }

  const renderComponent = (props) => {
    if (isAuthenticated) {
      if (isActive || ['/', '/rm-portal', '/rm-portal/'].includes(window.location.pathname)) {
        return (
          <Component {...props} />
        );
      } else {
        return goHome();
      }
    } else {
      if (ssoUrl) {
        return goToSsoUrl();
      }
    }
  };

  const render = props => renderComponent(props);

  return (
    <Route
      {...rest}
      render={render}
    />
  );
};

export default PrivateRoute;
