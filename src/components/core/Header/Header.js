import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import {
  AppBar,
  Grid,
  Toolbar,
  Link
} from '@material-ui/core';
import { 
  HomeOutlined
} from '@material-ui/icons';

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    background: 'linear-gradient(90deg, #355894 0%, #243961 100%)',
    borderBottom: '4px solid #CE722F',
  },
  headerImageWrapper: {
    paddingTop: '14px',
    paddingBottom: '14px',
    marginLeft: '2.5rem',
    marginRight: '2.5rem'
  },
  rmImage: {
    cursor: 'pointer',
    height: '32px'
  },
  homeLink: {
    color: 'white',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: 0,
    lineHeight: '18px',
    padding: '8px 24px'
  },
  homeIcon: {
    fontSize: '18px',
    marginRight: '3px',
    marginTop: '-1px'
  },
  ldImage: {
    height: '32px'
  }
});

const Header = (props) => {
  const { classes, homeType=null } = props;
  const imageClick = () => {
    props.history.push(`/`);
  } 

  return (
    <AppBar elevation={4} className={classes.appBar}>
      <Toolbar disableGutters>
        <Grid container item direction="row" justify="space-between" className={classes.headerImageWrapper}>
          <Grid item>
            <img 
              className={classes.rmImage} 
              alt="Revenue Management" 
              src="https://isistudio-public.s3.amazonaws.com/RM%20Logo%20final%20dark%20bg%403x.png"
              onClick={() => imageClick()}
            />
          </Grid>
          <Grid item>
            <Grid container direction='row'>
              {homeType &&
                <Link
                  component={RouterLink}
                  className={classes.homeLink}
                  to={homeType === 'learner' ? '/cases' : '/classes'}
                >
                  <span style={{verticalAlign: 'middle', display: 'inline-flex'}}>
                    <HomeOutlined className={classes.homeIcon}/>  HOME
                  </span>
                </Link>
              }
              <img 
                className={classes.ldImage}
                alt="Marriott L+D"
                src="https://isistudio-public.s3.amazonaws.com/WordMarks%403x.png"
              />
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

export default withRouter(withStyles(styles)(observer(Header)));
