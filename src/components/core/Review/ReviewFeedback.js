import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import classNames from 'classnames';

import { apiURL } from '../../../config';
import CECSimApi from '../../../api';

import {
  Typography,
  Grid,
  Button,
  Link,
  IconButton
} from '@material-ui/core';
import { PrintOutlined } from '@material-ui/icons';

import Header from '../Header/Header';
import Loading from '../Loading'

class ReviewFeedback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stepData: null,
      submission: null,
      recordingUrls: []
    };
  }

  async componentDidMount() {
    const { userStore } = this.props;
    const { stepDataId, subIndex } = this.props.match.params;

    let stepData;
    if (userStore.stepDatas) {
      stepData = userStore.getStepDataById(stepDataId);
    } else {
      stepData = (await CECSimApi.getStepData(stepDataId)).data;
    }
    const submission = stepData.submissions[subIndex];

    this.setState({
      stepData: stepData,
      submission: submission,
      recordingUrls: (submission.data.compositionIds || []).map(fileName => `${apiURL}/rm/recording/${fileName}`),
    });
  }


  onGoHomeClick = () => {
    this.props.history.push('/cases');
  }

  onDownload(fileName) {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
        const blobUrl = window.URL.createObjectURL(xmlHttp.response);
        const link = document.createElement('a');
        link.href = blobUrl;
        link.download = fileName;
        link.target = "_blank";
        link.rel = "noopener noreferrer";
        
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    };
    xmlHttp.responseType = 'blob';
    xmlHttp.open('GET', `${apiURL}/rm/recording/${fileName}`, true);
    xmlHttp.send(null);
  }

  async onGoToStepClick(stepId) {
    const { cases } = this.props.caseStore;
    const { stepData } = this.state;

    const stepName = cases.find(c => c._id === stepData.case._id).steps.find(s => s._id === stepId).name;

    this.props.history.push(`/case/${stepData.case._id}/${stepName}/${stepId}`);
  }

  render() {
    const { classes } = this.props;
    const { stepData, submission, recordingUrls } = this.state;
    let inputs, sections, recordingRounds, savedResponses, continueButton, stepNum;

    if (submission) {
      const caseSteps = stepData.case.stepData
      stepNum = stepData.step.order;
      
      const inputData = caseSteps[stepNum-1].data;
      inputs = inputData.inputs || [];
      sections = inputData.sections || [];
      recordingRounds = inputData.rounds;
      savedResponses = submission.data;

      let handleButtonClick, buttonText;

      if (stepData.status === 'Complete') {
        if (stepNum === caseSteps.length) {
          handleButtonClick = this.onGoHomeClick;
          buttonText = 'Close';
        } else {
          handleButtonClick = () => this.onGoToStepClick(caseSteps[stepNum].stepId);
          buttonText = 'Next Step';
        }
      } else {
        handleButtonClick = () => this.onGoToStepClick(caseSteps[stepNum-1].stepId);
        if (stepData.status === 'In Progress') {
          if (inputData.submissionType === 'group') {
            handleButtonClick = () => this.onGoToStepClick(inputData.groupStepIds[0]);
          }
          buttonText = 'Edit Step';
        } else if (stepData.status === 'Needs Review') {
          buttonText = 'Review';
        } else {
          console.log('Error: stepData status ' + stepData.status);
        }
      } 

      continueButton = <Button className={classes.button} onClick={handleButtonClick}>{buttonText}</Button>;
    }

    return (submission ?
      <Grid container className={classes.container}>
        <Grid item className={classes.printGrid}>
          <ReactToPrint 
            trigger={() => 
              <IconButton className={classes.iconButton}>
                <PrintOutlined className={classes.icon} />
              </IconButton>
            }
            content={() => this.componentRef}
          />
        </Grid>
        <Grid container item className={classes.printableContainer} ref={el => (this.componentRef = el)}>
          <Header className={classes.showOnPrint} homeType={'learner'}/>
          <Grid item xs={12}>
            <Typography variant="h3">{stepData.case.title}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1" className={classNames(classes.instructions, classes.hideOnPrint)}>Review feedback from your facilitator below.</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant='h5' className={classes.stepHeader}>Step {stepNum}: {stepData.step.title}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h3" className={classes.hideOnPrint}>{stepData.step.description}</Typography>
            <Typography variant="h3" className={classes.showOnPrint}>Learner Response:</Typography>
          </Grid>
          <Grid container item xs={12} className={classes.submissionContainer}>
            {/* Analyze, Decide */}
            {recordingUrls.length === 0 && sections.length === 0 && inputs.map((input, index) => 
              <Grid container item xs={12} className={classes.submissionItem} key={index}>
                <Grid item xs={12}>
                  <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                </Grid>
                <Grid item xs={12} className={classes.indentOnPrint}>
                  <Typography variant="body2" className={classes.response}>{savedResponses[input.name]}</Typography>
                </Grid>
              </Grid>
            )}

            {/* Prepare */}
            {recordingUrls.length === 0 && sections.length !== 0 && sections.map((section, index) => 
              <Grid container item xs={12} key={index} className={classes.submissionItem}>
                <Grid item xs={12}>
                  <Typography variant='body1' className={classes.sectionHeader}>{section.sectionName}</Typography>
                </Grid>
                {section.inputs.map((input, inputIndex) =>
                  <Grid container item xs={12} className={classes.submissionItem} key={inputIndex}>
                    <Grid item xs={12}>
                      <Typography variant="body1" className={classes.question}>{input.text}</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.indentOnPrint}>
                      <Typography variant="body2" className={classes.response}>{savedResponses[input.name]}</Typography>
                    </Grid>
                  </Grid>
                )}
              </Grid>
            )}

            {/* Record */}
            {recordingUrls.length > 0 && 
              <Grid container item xs={12} className={classes.submissionItem}>
                {/* Video Section */}
                <Grid item xs={12} className={classes.sectionHeader}>
                  <Typography variant='h3'>Video</Typography>
                </Grid>
                {recordingUrls.map((recordingUrl, index) => 
                  <Grid container item xs={8} key={index}>
                    <Grid item xs={12}>
                      <Typography variant="body1" className={classes.question}>
                        {recordingRounds[index].text}
                      </Typography>
                    </Grid>
                    <Grid item container xs={12} justify="center" className={classes.videoContainer}>
                      <video controls width="800" height="450" src={recordingUrl} type="video/webm" />
                    </Grid>
                  </Grid>
                )}
                {/* PPT Section */}
                {savedResponses.ppt &&
                  <Grid container item xs={12}>
                    <Grid item xs={12} className={classes.sectionHeader}>
                      <Typography variant='h3'>Powerpoint Presentation</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Link onClick={this.onDownload.bind(this, savedResponses.ppt.fileName)}>
                        {savedResponses.ppt.name}
                      </Link>
                    </Grid>
                  </Grid>
                }
              </Grid>
            }
          </Grid>
          <Grid container item xs={12} className={classes.feedBackGrid}>
            <Grid item xs={12} className={classes.hideOnPrint}>
              <Typography variant="h3" className={classes.question}>Feedback from your facilitator:</Typography>
            </Grid>
            <Grid item xs={12} className={classes.showOnPrint}>
              <Typography variant="h3" className={classes.question}>Facilitator Response:</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2" className={classes.response}>{submission.feedback ? submission.feedback.response : 'No feedback has been provided yet.'}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid container justify="flex-end">
          <Grid item className={classes.buttonGrid}>
            {continueButton}
          </Grid>
        </Grid>
      </Grid>
    : 
      <Loading />
    );
  }
}


const styles = theme => ({
  instructions: {
    marginBottom: '2rem'
  },
  stepHeader: {
    textTransform: 'uppercase',
    marginBottom: '2rem'
  },
  submissionItem: {
    marginTop: '1rem'
  },
  sectionHeader: {
    fontStyle: 'italic',
    fontWeight: '700'
  },
  question: {
    fontStyle: 'italic'
  },
  response: {
    marginTop: '.5rem',
    color: '#434343',
    whiteSpace: 'pre-wrap'
  },
  feedBackGrid: {
    marginTop: '2rem',
    marginBottom: '1rem'
  },
  textField: {
    width: '100%'
  },
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    marginBottom: '1rem',
    width: '100%',
    color: '#FFFFFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  },
  inputContainerGrid: {
    marginBottom: '2rem'
  },
  videoContainer: {
    marginTop: '1rem'
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#243961'
  },
  printGrid: {
    marginTop: '-50px'
  },
  showOnPrint: {
    display: 'none'
  },
  hideOnPrint: {
    display: 'block'
  },
  '@media print': {
    printableContainer: {
      marginTop: '64px',
      padding: '20px'
    },
    showOnPrint: {
      display: 'block'
    },
    hideOnPrint: {
      display: 'none'
    },
    indentOnPrint: {
      paddingLeft: '12px'
    },
    stepHeader: {
      marginTop: '1rem',
      marginBottom: '1rem'
    },
    submissionContainer: {
      marginTop: '-.5rem',
    },
    textLabel: {
      marginBottom: '0px'
    },
    prevTextLabel: {
      fontStyle: 'italic',
      fontWeight: 300,
      marginTop: '1rem'
    }
  }
});

export default withRouter(withStyles(styles)(inject('userStore', 'caseStore')(observer(ReviewFeedback))));