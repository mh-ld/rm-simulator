import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

import {
  Typography,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

import Loading from '../Loading'

const TableHeader = () => {
  const headings = ['Facilitator', 'Case Name', 'Step Name', 'Received Feedback?'];

  return (
    <TableHead>
      <TableRow>
        {headings.map((heading, index) => 
          <TableCell key={index}>
            <Typography variant="body1">
              <strong>{heading}</strong>
            </Typography>
          </TableCell>
        )}
      </TableRow>
    </TableHead>
  )
};

class Review extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stepDatas: [],
      isLoading: true
    };
  }

  async componentDidMount() {
    const { userStore } = this.props;
    this.setState({ isLoading: true });

    if (!userStore.stepDatas) {
      await userStore.fetchStepDatas();
    }

    this.setState({ 
      stepDatas: userStore.stepDatas.filter(sd => 
        !['none', 'wait'].includes(
          sd.case.stepData.find(step => 
            step.stepId.toString() === sd.step._id.toString()
          ).data.submissionType
        )
      ),
      isLoading: false
    });
  }


  onRowClick = (stepDataId, subIndex) => {
    this.props.history.push(`/submissions/${stepDataId}/${subIndex}`);
  }

  render() {
    const { classes } = this.props;
    const { stepDatas, isLoading } = this.state;
    
    return (!isLoading ?
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} className={classes.titleGrid}>
          <Typography variant="h3">
            Review Your Submissions
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Table>
            <TableHeader />
            <TableBody>
              {stepDatas && stepDatas.map(sd => 
                sd.submissions.filter(sub => sub.submitted).reverse().map((sub, subIndex) => 
                  <TableRow
                    key={sd._id+'/'+subIndex}
                    onClick={this.onRowClick.bind(this, sd._id, sd.submissions.filter(sub => sub.submitted).length - subIndex - 1)}
                    className={classes.tableRow}
                  >
                    <TableCell>
                      <Typography variant="body1">{sub.feedback && sub.feedback.facilitator.name}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1">{sd.case.title}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1">{sd.step.title}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1">{sub.feedback ? 'Yes' : 'No'}</Typography>
                    </TableCell>
                  </TableRow>
                )
              )}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    : 
      <Loading />
    );
  }
}


const styles = theme => ({
  container: {
    
  },
  titleGrid: {
    paddingTop: 20,
    paddingBottom: 20
  },
  tableRow: {
    cursor: 'pointer'
  }
});

export default withRouter(withStyles(styles)(inject('userStore')(observer(Review))));