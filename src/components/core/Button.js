import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const DLZButton = React.forwardRef(({ textAlign, secondary, variant, children, classes, ...props }, ref) => {
  const className = classNames(classes.button, props.className, secondary && classes.secondary, props.className);
  let justify;

  switch (textAlign) {
    case 'center':
      justify = 'center'
      break;
    case 'right':
      justify = 'flex-end';
      break;
    default:
      justify = 'flex-start';
      break;
  }

  return (
    <Button
      {...props}
      ref={ref}
      className={className}
      variant={variant || 'contained'}
      color={props.color || 'primary'}
      size={props.size || 'medium'}
      style={{ justifyContent: justify, ...props.style }}
    >
      {children}
    </Button>
  );
});

const styles = theme => ({
  button: {
    borderRadius: 0,
    '&:focus': {
      border: '1px solid #51abff'
    },
    height: 48,
    boxShadow: 'none',
    '&:hover': {
      background: '#fefefe !important',
      color: 'black'
    }
  },
  secondary: {
    backgroundColor: 'white',
    color: 'black',
    border: `1px solid black`
  }
});

export default withStyles(styles)(DLZButton);
