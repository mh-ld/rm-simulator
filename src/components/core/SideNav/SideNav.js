import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import {
  Typography,
  Drawer,
  Stepper,
  Step,
  StepLabel,
  StepButton
} from '@material-ui/core';
import { EditOutlined } from '@material-ui/icons';

const drawerWidth = '18rem';

const styles = theme => ({
  drawerPaper: {
    width: drawerWidth,
    background: 'linear-gradient(90deg, #355894 0%, #243961 100%)',
    boxShadow: '0 2px 4px 0 rgba(0,0,0,0.5)',
    marginTop: '64px',
    height: 'calc(100% - 64px)',
    paddingTop: theme.spacing.unit * 6,
    paddingLeft: theme.spacing.unit * 5,
    paddingRight: theme.spacing.unit * 5,
    position: 'relative',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    padding: 0,
    overflowX: 'hidden',
    width: 0,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })
  },
  drawerContent: {
    
  },
  header: {
    color: '#FFFFFF',
    marginBottom: '.5rem'
  },
  subheader: {
    color: '#FFFFFF',
    fontStyle: 'italic',
    marginBottom: '1rem'
  },
  description: {
    color: '#FFFFFF'
  },
  stepper: {
    padding: '0px',
    marginTop: theme.spacing.unit * 5,
    background: 'none'
  },
  stepButton: {
    margin: '-5px',
    padding: '5px',
    borderRadius: '10px'
  },
  editIconWrapper: {
    backgroundColor: '#CE722F',
    borderRadius: '50%',
    height: '35px',
    width: '35px',
  },
  editIcon: {
    color: '#FFF',
    padding: '15%'
  }
});

const SideNav = (props) => {
  const { classes, currentCase, currentStepIndex, learnerView } = props;
  
  const stepIcon = (step) => {
    if (step.status === 'In Progress' && step.submissions.length > 0) {
      return(() => 
        <div className={classes.editIconWrapper}>
          <EditOutlined className={classes.editIcon}/>
        </div>
      );
     }
     return null;
  }

  const goToStep = (step) => {
    props.history.push(`/case/${currentCase._id}/${step.name}/${step._id}`);
  }

  return (
    <Drawer variant="persistent" classes={{ paper: classes.drawerPaper }} open={true}>
      { learnerView && currentCase && (
        <div className={classes.drawerContent}>
          <Typography variant="h2" className={classes.header}>
            {props.navHeader}
          </Typography>
          <Typography variant="h2" className={classes.subheader}>
            {props.navSubheader}
          </Typography>
          <Typography variant="body1" className={classes.description}>
            {props.navIntro}
          </Typography>

          {/* New */}
          <Stepper 
            activeStep={currentStepIndex}
            orientation="vertical"
            className={classes.stepper}
            nonLinear
          >
            { currentCase.steps.map((step, index) => (
              <Step key={index} completed={step.completed}>
                <StepButton 
                  onClick={() => goToStep(step)}
                  disabled={!step.completed && !step.priorStepCompleted}
                  className={classes.stepButton}
                >
                  <StepLabel StepIconComponent={stepIcon(step)}>
                    {`STEP ${index + 1}: ${step.title}`}
                  </StepLabel>
                </StepButton>
              </Step>
            ))}
          </Stepper>
        </div>
      )}
    </Drawer>
  );

}

export default withRouter(withStyles(styles)(observer(SideNav)));
