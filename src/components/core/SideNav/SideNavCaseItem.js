//TO DO: delete this fiel

import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { ListItemIcon } from '@material-ui/core';
import PlayIcon from '@material-ui/icons/PlayCircleOutline';
import { Route } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const SideNavCaseItem = (props) => (
  <Route
    path={`/case/${props._id}`}
    children={({ match }) => (
      <ListItem selected={!!match} button onClick={props.openCase}>
        <ListItemIcon>
          <PlayIcon />
        </ListItemIcon>
        <ListItemText primary={props.caseLabel} secondary={props.title} />

        <br />
        <div style={{ position: 'absolute', 'bottom': 0, left: -10, width: '100%' }}>
          <LinearProgress style={{ width: '100%' }} variant="determinate" value={getRandomInt(0, 100)} />
        </div>

      </ListItem>
    )}
  />

);

export default SideNavCaseItem;