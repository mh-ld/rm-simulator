import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import moment from 'moment';

import CECSimApi from '../../../api';

import {
  Typography,
  Grid,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

import { DeleteOutlined, AddCircleOutlineOutlined } from '@material-ui/icons';
import DeleteClassModal from './DeleteClassModal';
import Loading from '../Loading'

const styles = theme => ({
  description: {
    marginBottom: '2rem'
  },
  classGrid: {
    marginTop: '2rem',
  },
  newClassGrid: {
    marginTop: '1rem'
  },
  tableRow: {
    cursor: 'pointer'
  },
  bodyText: {
    color: '#494949'
  },
  closedBodyText: {
    color: '#494949',
    fontStyle: 'italic',
  },
  newClassText: {
    color: '#CE722F'
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#CE722F',
  },
});

const TableHeader = () => {
  const headings = ['Class', 'Start Date', 'Facilitators', ''];

  return (
    <TableHead>
      <TableRow>
        {headings.map((heading, index) => (
          <TableCell key={index}>
            <Typography variant="body1">
              <strong>{heading}</strong>
            </Typography>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
};

class ClassList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDeleteClassModal: false,
      rmClasses: [],
      deleteCid: '',
      isLoading: true
    };
  }

  async componentDidMount() {
    const { isAdmin } = this.props.userStore;
    this.setState({ isLoading: true });

    const { data: rmClasses } = await CECSimApi.getMyClasses(isAdmin);

    this.setState({
      ...this.state,
      rmClasses: rmClasses,
      isLoading: false
    });
  }

  onRowClick = (classIndex) => {
    const { userStore, history } = this.props;

    const rmClass = this.state.rmClasses[classIndex];
    history.push(`/class/${rmClass._id}/${userStore.isAdmin ? 'edit' : 'submissions'}`);
  }

  onDeleteClick = async (classIndex) => {
    const classId = this.state.rmClasses[classIndex]._id;

    this.setState({
      showDeleteClassModal: true,
      deleteCid: classId
    });
  }

  onDeleteConfirmed = async () => {
    const { isAdmin } = this.props.userStore;
    const { deleteCid } = this.state;

    await CECSimApi.removeClass(deleteCid);
    this.setState({ isLoading: true });
    const { data: rmClasses } = await CECSimApi.getMyClasses(isAdmin);

    this.setState({
      ...this.state,
      rmClasses: rmClasses,
      showDeleteClassModal: false,
      deleteCid: '',
      isLoading: false
    });
  }

  onDeleteClassModalClosed = async () => {
    this.setState({
      showDeleteClassModal: false,
      deleteCid: ''
    });
  }

  onAddClass = () => {
    this.props.history.push(`/class/new/edit`);
  }

  render() {
    const { classes, userStore } = this.props;
    const { showDeleteClassModal, rmClasses, isLoading } = this.state;
    
    return (!isLoading ?
      <Grid container>
        <Grid container item xs={12}>
          <Grid item xs={12}>
            <Typography variant='h3'>Class View</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant='body1' className={classes.description}>
              {userStore.isAdmin ?
                'Select an existing class to edit or create a new one from the table below.'
                : 'Select from your existing classes to review and provide feedback to participants'
              }
            </Typography>
          </Grid>
          <Grid container item xs={12} className={classes.classGrid}>
            <Table>
              <TableHeader />
              <TableBody>
                {rmClasses.map((c, index) => (
                  <TableRow key={index}>
                    <TableCell onClick={this.onRowClick.bind(this, index)} className={classes.tableRow}>
                      <Typography variant="body1" className={(!c.closed ? classes.bodyText : classes.closedBodyText)}>
                        {c.name}
                      </Typography>
                    </TableCell>
                    <TableCell onClick={this.onRowClick.bind(this, index)} className={classes.tableRow}>
                      <Typography variant="body1" className={(!c.closed ? classes.bodyText : classes.closedBodyText)}>
                        {moment(c.startDate).format('D MMMM YYYY') + (c.closed ? " - Closed" : "")}
                      </Typography>
                    </TableCell>
                    <TableCell onClick={this.onRowClick.bind(this, index)} className={classes.tableRow}>
                      <Typography variant="body1" className={(!c.closed ? classes.bodyText : classes.closedBodyText)}>
                        {c.facilitators.map(f => f.name).toString()}
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      {userStore.isAdmin &&
                        <IconButton className={classes.iconButton} onClick={this.onDeleteClick.bind(this, index)}>
                          <DeleteOutlined className={classes.icon} />
                        </IconButton>
                      }
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Grid>
          {userStore.isAdmin &&
            <Grid container item xs={12} className={classes.newClassGrid}>
              <Table>
                <TableHead>
                  <TableRow onClick={this.onAddClass.bind(this)} className={classes.tableRow}>
                    <TableCell>
                      <Typography variant="body1" className={classes.newClassText}>
                        Add New Class
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      <IconButton className={classes.iconButton}>
                        <AddCircleOutlineOutlined className={classes.icon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableHead>
              </Table>
          </Grid>  
          }
        </Grid>
        {showDeleteClassModal &&
          <DeleteClassModal
            handleClose={this.onDeleteClassModalClosed}
            open={showDeleteClassModal}
            handleDelete={this.onDeleteConfirmed}
          />
        }
      </Grid>
    : 
      <Loading />
    );
  }
}

export default withRouter(withStyles(styles)(inject('caseStore')(observer(ClassList))));