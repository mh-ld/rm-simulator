import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Button,
  Grid,
  Dialog,
  Link
} from '@material-ui/core';

const styles = theme => ({
  dialogPaper: {
    backgroundColor: '#434343',
    borderRadius: '8px',
    boxShadow: 'none',
    overflowX: 'hidden'
  },
  dialogContent: {
    padding: '1rem 1rem',
  },
  dialogBody: {
    paddingTop: '2rem',
    paddingBottom: '2rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
  },
  headingText: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '24px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '29px',
    marginBottom: '1rem',
  },
  description: {
    color: '#FFF',
    fontFamily: 'Calibri',
    fontSize: '18px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '23px',
    marginBottom: '2rem'
  },
  centerOnGrid: {
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#CE722F',
    color: '#FFFFFF',
    fontFamily: 'Calibri',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
    marginBottom: '2rem',
  },
  deleteLink: {
    color: '#FFF',
    fontFamily: 'Calibri',
    fontSize: '18px',
    fontWeight: 300,
    letterSpacing: 0,
    lineHeight: '23px',
  }
});

class DeleteClassModal extends Component {

  render() {
    const { classes, handleClose, open, handleDelete } = this.props;

    return (
      <Dialog
        open={open}
        classes={{ paper: classes.dialogPaper }}
        maxWidth={'md'}
      >
        {
          open ?
            <Grid container justify="center" className={classes.dialogContent}>
              <Grid container justify="center" className={classes.dialogBody}>
                <Grid item container xs={12} direction="row">
                  <Grid item xs={12}>
                    <Typography className={classes.headingText} align="center">
                      Do you really want to delete this class?
                    </Typography>
                    <Typography className={classes.description} align="center">
                      Deleting this class will remove it permanently, as well as revoke access to the RM Portal from all of its participants.
                    </Typography>
                    <Typography className={classes.description} align="center">
                      This cannot be undone.
                    </Typography>
                  </Grid>
                  <Grid item xs={12} className={classes.centerOnGrid}>
                    <Button onClick={handleClose} className={classes.button}>
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item xs={12} className={classes.centerOnGrid}>
                    <Link className={classes.deleteLink} onClick={handleDelete}>Yes. Delete This Class.</Link>
                  </Grid>
                </Grid>
              </Grid>
            </Grid> : <div />
        }
      </Dialog>
    );
  }

}

export default withStyles(styles)(DeleteClassModal);