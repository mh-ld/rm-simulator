import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import moment from 'moment';

import CECSimApi from '../../../api';

import {
  Grid,
  Typography,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

const styles = theme => ({
  submissionsGrid: {
    backgroundColor: 'white',
  },
  clickableTableRow: {
    cursor: 'pointer'
  },
  bodyText: {
    color: '#494949'
  },
  highlightedBodyText: {
    color: '#CE722F'
  },
  textField: {
    width: '100%'
  },
  textLabel: {
    marginBottom: '.5rem',
    marginTop: '2rem',
  },
});

const Input = ({ classes, value, name }) => {
  return (
    <Grid container item xs={12} className={classes.inputGrid}>
      <Grid item xs={12}  >
        <Typography variant="body2" className={classes.textLabel}>{name}</Typography>
      </Grid>
      <Grid item xs={12}>
        <TextField
          name={name}
          value={value}
          disabled
          className={classes.textField}
          variant="outlined"
          margin='none'
        />
      </Grid>
    </Grid>
  );
}

const TableHeader = () => {
  const headings = ['Learner', 'Case', 'Step', 'Last Reviewed By', 'Status'];

  return (
    <TableHead>
      <TableRow>
        {headings.map((heading, index) => (
          <TableCell key={index}>
            <Typography variant="body1">
              <strong>{heading}</strong>
            </Typography>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
};

class ClassSubmissions extends Component {

  constructor(props) {
    super(props);

    this.state = {
      partStepDatas: [],
    };
  }

  async componentDidMount() {
    const { participants, caseStore } = this.props;
    const { classId } = this.props.match.params;
    
    const { data: stepDatas } = await CECSimApi.getLatestClassStepDatas(classId);

    const partStepDatas = participants.map(p => {
      let stepData = stepDatas.find(sd => sd.learnerEid === p.eid);

      if (!stepData) {
        stepData = {
          case: caseStore.cases[0],
          step: caseStore.cases[0].steps[0],
          submissions: [],
          status: 'Not Started'
        }
      }

      stepData.lastReviewedBy = '';
      const numSubs = stepData.submissions.length;
      if (numSubs > 0) {
        const lastSub = stepData.submissions[numSubs-1];
        if (lastSub.feedback) {
          stepData.lastReviewedBy = lastSub.feedback.facilitator.name;
        } else if (numSubs > 1) {
          stepData.lastReviewedBy = stepData.submissions[numSubs-2].feedback.facilitator.name;
        }
      }

      return ({ 
        ...p, 
        stepData
      });
    });

    const statusOrder = ['Needs Review', 'In Progress', 'Complete', 'Not Started'];

    this.setState({ 
      partStepDatas: partStepDatas.sort((a, b) => statusOrder.indexOf(a.status) - statusOrder.indexOf(b.status))
    });
  }

  onRowClick = (stepData) => {
    this.props.history.push(`/class/${this.props.match.params.classId}/submission/${stepData._id}`);
  }

  render() {
    const { classes, name, startDate } = this.props;
    const { partStepDatas } = this.state;

    return (
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h5'>REVIEW LEARNER SUBMISSIONS</Typography>
          <Grid container item xs={12}>
            <Input
              classes={classes}
              value={name}
              name='Class Name'
            />
            <Grid container item xs={5}>
              <Input
                classes={classes}
                value={moment(startDate).format('D MMMM YYYY')}
                name='Start Date'
              />
            </Grid>
          </Grid>
          <Grid item xs={12}  >
            <Typography variant="body2" className={classes.textLabel}>Class Submissions</Typography>
          </Grid>
          <Grid container item xs={12} className={classes.submissionsGrid}>
            <Table>
              <TableHeader />
                <TableBody>
                  {partStepDatas.map(p => {
                    const stepData = p.stepData;
                    const hasSubmissions = stepData.submissions.length > 1 || (stepData.submissions.length === 1 && stepData.submissions[0].submitted);

                    return (
                      <TableRow 
                        key={p.eid} 
                        onClick={hasSubmissions ? this.onRowClick.bind(this, stepData) : undefined} 
                        className={hasSubmissions ? classes.clickableTableRow : null}
                      >
                        {[p.name, stepData.case.title, stepData.step.title, stepData.lastReviewedBy, stepData.status].map((field, index) => 
                          <TableCell key={index}>
                            <Typography 
                              variant="body1" 
                              className={stepData.status === 'Needs Review' ? classes.highlightedBodyText : classes.bodyText}
                            >
                              {field}
                            </Typography>
                          </TableCell>
                        )}
                      </TableRow>
                    );
                  })}
                </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withRouter(withStyles(styles)(inject('caseStore')(observer(ClassSubmissions))));