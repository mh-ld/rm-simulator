import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import moment from 'moment';

import CECSimApi from '../../../api';

import {
  Typography,
  Button,
  Grid,
  IconButton,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Link,
  Tooltip
} from '@material-ui/core';

import { DeleteOutlined, AddCircleOutlineOutlined, HelpOutlineOutlined, EditOutlined } from '@material-ui/icons';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DeleteUserModal from './DeleteUserModal';
import NewUserModal from './NewUserModal';
import CloseClassModal from './CloseClassModal';
import Loading from '../Loading'

const styles = theme => ({
  rosterGrid: {
    backgroundColor: 'white',
  },
  clickableRow: {
    cursor: 'pointer'
  },
  bodyText: {
    color: '#494949'
  },
  newUserGrid: {
    marginTop: '1rem',
    backgroundColor: 'white',
  },
  newUserText: {
    color: '#CE722F'
  },
  fieldGrid: {
    position: 'relative'
  },
  editIconButton: {
    position: 'absolute',
    padding: '16px 14px',
    right: 0,
    zIndex: 999
  },
  addFacilsIconButton: {
    marginTop: '-14.5px'
  },
  editIcon: {
    color: '#434343'
  },
  iconButton: {
    padding: '0px'
  },
  icon: {
    color: '#CE722F',
  },
  textField: {
    width: '100%'
  },
  addFacilsButton: {
    backgroundColor: '#FFF',
    border: '1px solid #BFBECD',
    borderRadius: '4px',
    cursor: 'pointer',
    height: '56px',
    paddingTop: '18.5px',
    paddingBottom: '18.5px',
    paddingLeft: '14px',
    paddingRight: '2px',
    transition: 'border .25s',
    '&:hover': {
      borderColor: '#000',
    }
  },
  addFacilsButtonDisabled: {
    backgroundColor: 'rgb(229, 229, 229)',
    border: '1px solid rgba(0, 0, 0, 0.26)',
    borderRadius: '4px',
    height: '56px',
    paddingTop: '18.5px',
    paddingBottom: '18.5px',
    paddingLeft: '14px',
    paddingRight: '14px',
  },
  addFacilsText: {
    color: 'rgb(143,143,143)',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '16px'
  },
  textLabel: {
    marginBottom: '.5rem',
    marginTop: '2rem',
  },
  manageFacilsLabel: {
    marginTop: '3rem',
    marginBottom: '-1rem'
  },
  datePicker: {
    width: '100%',
  },
  buttonGrid: {
    marginTop: '1rem'
  },
  button: {
    marginTop: '1rem',
    marginBottom: '1rem',
    width: '100%',
    color: '#FFFFFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    letterSpacing: '0',
    lineHeight: '18px',
  },
  outlinedButton: {
    backgroundColor: 'transparent',
    border: 'solid 1px #355894',
    color: '#355894',
  },
  error: {
    color: 'red',
  },
  closeClassGrid: {
    marginTop: '1rem'
  },
  closeClassLink: {
    color: '#243961'
  },
  radioGroupLabel: {
    color: '#243961',
    fontSize: '15px'
  },
});

const TableHeader = () => {
  const headings = ['EID', 'Name', ''];

  return (
    <TableHead>
      <TableRow>
        {headings.map((heading, index) => (
          <TableCell key={index}>
            <Typography variant="body1">
              <strong>{heading}</strong>
            </Typography>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
};

class LocalizedUtils extends MomentUtils {
  getDatePickerHeaderText(date) {
    return moment(date).format('D MMMM YYYY');
  }
}

class EditClass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      currentClass: {
        name: '',
        startDate: new Date(),
        facilitators: [],
        participants: [],
      },
      minDate: new Date(),
      showDeleteUserModal: false,
      showNewUserModal: false,
      showCloseClassModal: false,
      userType: '',
      emptyFields: false,
      saveDisabled: false
    }
  }

  async componentDidMount() {
    const { editType } = this.props;

    if (editType === 'edit') {
      this.setState({ loading: true });

      const { data: lookupClass } = await CECSimApi.getClass(this.props.match.params.classId);

      this.setState({ 
        ...this.state,
        minDate: new Date() < lookupClass.startDate ? new Date() : lookupClass.startDate,
        currentClass: {
          name: lookupClass.name,
          startDate: lookupClass.startDate,
          facilitators: lookupClass.facilitators,
          participants: lookupClass.participants,
          closed: lookupClass.closed,
        },
        loading: false
      });
    } else {
      this.setState({ loading: false });
    }
  }

  onDeleteClick = async (eid, userType) => {
    this.setState({ 
      showDeleteUserModal: true,
      deleteEid: eid,
      userType: userType
    });
  }

  onDeleteConfirmed = async () => {
    const { currentClass, deleteEid, userType } = this.state;

    if (userType === 'participant') {
      this.setState({ 
        currentClass: {
          ...currentClass,
          participants: currentClass.participants.filter(p => p.eid !== deleteEid)
        }
       });
    } else {
      this.setState({ 
        currentClass: {
          ...currentClass,
          facilitators: currentClass.facilitators.filter(f => f.eid !== deleteEid)
        }
       });
    }

    this.setState({ 
      showDeleteUserModal: false,
      deleteEid: '',
      userType: ''
     });
  }

  onDeleteModalClosed = async () => {
    this.setState({ 
      showDeleteUserModal: false,
      deleteEid: '',
      userType: ''
    });
  }

  onAddClick = (userType) => {
    this.setState({ 
      showNewUserModal: true,
      userType: userType
    });
  }

  onAddConfirmed = async (usersToAdd) => {
    const { currentClass, userType } = this.state;

    try {
      let newUsers = [];

      if (userType === 'participant') {
        newUsers = currentClass.participants;
      } else {
        newUsers = currentClass.facilitators;
      }

      newUsers = newUsers.concat(usersToAdd
        .filter(user => !newUsers.some(u => u.eid === user.eid))
        .map(user => ({
          user: user._id,
          eid: user.eid,
          name: user.name
        }))
      );

      if (userType === 'participant') {
        this.setState({ 
          currentClass: {
            ...currentClass,
            participants: newUsers
          }
        });
      } else {
        this.setState({ 
          currentClass: {
            ...currentClass,
            facilitators: newUsers
          }
        });
      }
    } catch(err) {
      console.log(err);
    }
    this.setState({ showNewUserModal: false });
  }

  onAddModalClosed = async () => {
    this.setState({
      showNewUserModal: false, 
      userType: '' 
    });
  }

  onCloseClassClick = async (eid, userType) => {
    this.setState({ 
      showCloseClassModal: true
    });
  }

  onCloseClassConfirmed = async () => {
    try {
      await CECSimApi.closeClass(this.props.match.params.classId);
      this.props.history.push(`/classes`);
    } catch(err) {
      console.log(err);
    }
  }

  onCloseClassModalClosed = async () => {
    this.setState({ 
      showCloseClassModal: false
    });
  }

  onChange = (event) => {
    this.setState({
      currentClass: {
        ...this.state.currentClass,
        [event.target.name]: event.target.value
      }
    });
  }

  onDateChange = (date) => {
    this.setState({
      currentClass: {
        ...this.state.currentClass,
        startDate: date
      }
    });
  }

  onSave = async () => {
    const { currentClass } = this.state;
    const { editType } = this.props;

    this.setState({ saveDisabled: true });

    try {
      if (this.noEmptyFields()) {
        if (editType === 'new') {
          await CECSimApi.createClass(
            currentClass.name,
            currentClass.startDate,
            currentClass.facilitators,
            currentClass.participants,
          );
        } else {
          await CECSimApi.editClass(
            currentClass.name,
            currentClass.startDate,
            currentClass.facilitators,
            currentClass.participants, 
            this.props.match.params.classId
          );
        }
        this.props.history.push(`/classes`);
      } else {
        console.log('Error: empty fields exist');
      }
    } catch(err) {
      console.log(err);
    }
    
    this.setState({ saveDisabled: false });
  }

  noEmptyFields = () => {
    const { currentClass } = this.state;
    const emptyFields = currentClass.name === '' || currentClass.facilitators.length === 0;

    this.setState({ emptyFields: emptyFields });
    return !emptyFields;
  }

  onCancel = async () => {
    this.props.history.push(`/classes`);
  }

  render() {
    const { classes, editType } = this.props; 
    const { 
      currentClass, 
      showDeleteUserModal, 
      showNewUserModal,
      showCloseClassModal,
      userType,
      emptyFields,
      saveDisabled,
      minDate,
      loading
    } = this.state;

    return (!loading ?
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h5'>
            {editType === 'new' ? 'CREATE NEW CLASS' : 'VIEW OR EDIT CLASS'}
          </Typography>
          <Grid container item xs={12}>
            <Grid container item xs={12}>
              <Grid item xs={12}  >
                <Typography variant="body2" className={classes.textLabel}>Class Name</Typography>
              </Grid>
              <Grid item xs={12} className={classes.fieldGrid}>
                <TextField
                  onChange={this.onChange}
                  name={'name'}
                  value={currentClass.name}
                  className={classes.textField}
                  disabled={currentClass.closed}
                  variant="outlined"
                  margin='none'
                />
                {!currentClass.closed &&
                  <IconButton disabled className={classes.editIconButton}>
                    <EditOutlined className={classes.editIcon} />
                  </IconButton>
                }
              </Grid>
            </Grid>
            <Grid container item xs={5}>
              <Grid item xs={12}>
                <Typography variant="body2" className={classes.textLabel}>Start Date</Typography>
              </Grid>
              <MuiPickersUtilsProvider utils={LocalizedUtils}>
                <Grid item xs={12} className={classes.fieldGrid}>
                  <DatePicker
                    className={classes.datePicker}
                    variant="outlined"
                    value={currentClass.startDate}
                    disabled={currentClass.closed}
                    format='D MMMM YYYY'
                    minDate={minDate}
                    onChange={this.onDateChange}
                  />
                  {!currentClass.closed &&
                    <IconButton disabled className={classes.editIconButton}>
                      <EditOutlined className={classes.editIcon} />
                    </IconButton>
                  }
                </Grid>
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid container item xs={2}></Grid>
            <Grid container item xs={5}>
              <Grid item xs={12}  >
                <Typography variant="body2" className={classes.textLabel}>Facilitators</Typography>
              </Grid>
              <Grid item xs={12} className={classes.fieldGrid}>
                <Grid 
                  container
                  item
                  xs={12} 
                  className={!currentClass.closed ? classes.addFacilsButton : classes.addFacilsButtonDisabled}
                  onClick={!currentClass.closed && this.onAddClick.bind(this, 'facilitator')}
                  justify='space-between'
                >
                  <Typography className={classes.addFacilsText}>
                    Add Facilitators...
                  </Typography>
                  {!currentClass.closed &&
                    <IconButton className={classes.addFacilsIconButton}>
                      <EditOutlined className={classes.editIcon} />
                    </IconButton>
                  }
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body2" className={classes.textLabel}>Class Roster</Typography>
          </Grid>
          <Grid container item xs={12} className={classes.rosterGrid}>
            <Table>
              <TableHeader />
              <TableBody>
                {currentClass.participants.map((p, index) =>
                  <TableRow key={index} className={classes.tableRow}>
                    <TableCell>
                      <Typography variant="body1" className={classes.bodyText}>
                        {p.eid}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" className={classes.bodyText}>
                        {p.name}
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      {!currentClass.closed &&
                        <IconButton className={classes.iconButton} onClick={this.onDeleteClick.bind(this, p.eid, 'participant')}>
                          <DeleteOutlined className={classes.icon} />
                        </IconButton>
                      }
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </Grid>
          {!currentClass.closed &&
            <Grid container item xs={12} className={classes.newUserGrid}>
              <Table>
                <TableHead>
                  <TableRow className={classes.clickableRow} onClick={this.onAddClick.bind(this, 'participant')}>
                    <TableCell>
                      <Typography variant="body1" className={classes.newUserText}>
                        Add New Participants
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      <IconButton className={classes.iconButton}>
                        <AddCircleOutlineOutlined className={classes.icon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableHead>
              </Table>
            </Grid>
          }

          <Typography variant='h5' className={classes.manageFacilsLabel}>
            MANAGE FACILITATORS
          </Typography>
          <Grid item xs={12}>
            <Typography variant="body2" className={classes.textLabel}>Facilitators</Typography>
          </Grid>
          <Grid container item xs={12} className={classes.rosterGrid}>
            <Table>
              <TableHeader />
              <TableBody>
                {currentClass.facilitators.map((f, index) =>
                  <TableRow key={index} className={classes.tableRow}>
                    <TableCell>
                      <Typography variant="body1" className={classes.bodyText}>
                        {f.eid}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" className={classes.bodyText}>
                        {f.name}
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      {!currentClass.closed &&
                        <IconButton className={classes.iconButton} onClick={this.onDeleteClick.bind(this, f.eid, 'facilitator')}>
                          <DeleteOutlined className={classes.icon} />
                        </IconButton>
                      }
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </Grid>
          {!currentClass.closed &&
            <Grid container item xs={12} className={classes.newUserGrid}>
              <Table>
                <TableHead>
                  <TableRow className={classes.clickableRow} onClick={this.onAddClick.bind(this, 'facilitator')}>
                    <TableCell>
                      <Typography variant="body1" className={classes.newUserText}>
                        Add New Facilitators
                      </Typography>
                    </TableCell>
                    <TableCell align='right'>
                      <IconButton className={classes.iconButton}>
                        <AddCircleOutlineOutlined className={classes.icon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableHead>
              </Table>
            </Grid>
          }
          {!currentClass.closed &&
            <Grid container justify="space-between" className={classes.buttonGrid}>
              <Grid item className={classes.closeClassGrid}>
                {editType === 'edit' &&
                  <Link className={classes.closeClassLink} underline='always' onClick={this.onCloseClassClick}>
                    <span>
                      Close Class <Tooltip 
                        placement='top'
                        arrow='true'
                        title="Closing this class will revoke access to the RM Portal from all of its participants. This cannot be undone."
                      >
                        <HelpOutlineOutlined className={classes.radioGroupLabel}/>
                      </Tooltip>
                    </span>
                  </Link>
                }
              </Grid>
              <Grid item>
                <Button className={classes.button} onClick={this.onSave} disabled={saveDisabled}>
                  Save
                </Button>
                {emptyFields &&
                  <Typography variant='body1' className={classes.error}>
                    The class must have a name and at least one facilitator.
                  </Typography>
                }
                {editType === 'new' &&
                  <Button className={classNames(classes.button, classes.outlinedButton)} onClick={this.onCancel}>
                    Cancel
                  </Button>
                }
              </Grid>
            </Grid>
          }
        </Grid>
        {showDeleteUserModal &&
          <DeleteUserModal
            open={showDeleteUserModal}
            handleDelete={this.onDeleteConfirmed}
            handleClose={this.onDeleteModalClosed}
            userType={userType}
          />
        }
        {showNewUserModal &&
          <NewUserModal
            handleAdd={this.onAddConfirmed}
            handleClose={this.onAddModalClosed}
            userType={userType}
            currentUsers={userType === 'participant' ? currentClass.participants : currentClass.facilitators}
          />
        }
        {showCloseClassModal &&
          <CloseClassModal
            open={showCloseClassModal}
            handleCloseClass={this.onCloseClassConfirmed}
            handleCancel={this.onCloseClassModalClosed}
          />
        }
      </Grid>
    : 
      <Loading />
    );
  }
}

export default withRouter(withStyles(styles)(inject('userStore')(observer(EditClass))));