import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Button,
  Grid,
  Dialog,
  IconButton,
  TextField,
  FormControlLabel,
  Checkbox
} from '@material-ui/core';
import { CloseOutlined, SearchOutlined } from '@material-ui/icons';

import Loading from '../Loading'

import CECSimApi from '../../../api';

const styles = theme => ({
  dialogPaper: {
    backgroundColor: '#434343',
    borderRadius: '8px',
    boxShadow: 'none',
    overflow: 'hidden'
  },
  dialogContent: {
    paddingTop: '16px',
    paddingLeft: '24px',
    paddingRight: '24px',
    paddingBottom: '32px',
    width: '787px'
  },
  headerGrid: {
    width: '100%',
  },
  headerText: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Light',
    fontSize: '18px',
    letterSpacing: .7,
    lineHeight: '18px',
    textTransform: 'uppercase'
  },
  iconButton: {
    padding: '0px'
  },
  closeIcon: {
    color: '#FFF',
    fontSize: '18px'
  },
  userBox: {
    backgroundColor: '#BFBECD',
    borderRadius: '5px',
    padding: '8px'
  },
  userBoxName: {
    color: '#243961',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '14px',
    lineHeight: '18px'
  },
  deleteUserIcon: {
    color: '#243961',
    fontSize: '14px'
  },
  searchLabelGrid: {
    marginBottom: '4px'
  },
  searchLabel: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Light',
    fontSize: '14px',
    letterSpacing: .7,
    lineHeight: '18px',
  },
  searchBarGrid: {
    position: 'relative'
  },
  searchBar: {
    width: '100%'
  },
  searchIconButton: {
    position: 'absolute',
    padding: '16px 14px',
    right: 0,
    zIndex: 999
  },
  searchIcon: {
    color: '#434343'
  },
  searchResult: {
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '14px',
    lineHeight: '18px',
  },
  disabledSearchResult: {
    color: '#BBB',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '14px',
    lineHeight: '18px',
  },
  searchResultCheckbox: {
    paddingTop: '0px',
    paddingBottom: '0px'
  },
  button: {
    backgroundColor: '#CE722F',
    color: '#FFF',
    fontFamily: 'ProximaNova-Regular',
    fontSize: '15px',
    lineHeight: '18px',
    textTransform: 'none',
    padding: '12px 48px'
  }
});

class NewUserModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      usersToAdd: [],
      searchTerm: '',
      searchResults: []
    };
  }

  async componentDidMount() {
    this.setState({
      usersToAdd: [],
      searchTerm: '',
      isSearching: false,
      searchResults: []
    });
  }

  onSearchChange = (event) => {
    this.setState({
      searchTerm: event.target.value
    });
  }

  onSearch = async () => {
    const { searchTerm } = this.state;
    const trimmed = searchTerm.trim();

    if (trimmed === '' || trimmed.length < 2) {
      this.setState({ searchResults: [] });
      return;
    }

    try {
      this.setState({ isSearching: true });
      const { data } = await CECSimApi.searchUsers(trimmed, 15);

      this.setState({ 
        searchResults: (data ? data : []),
        isSearching: false
      });
    } catch (e) {
      this.setState({ 
        searchResults: [],
        isSearching: false
      });
      console.log(e);
    }
  }

  onKeyPress = (event) => {
    if (event.key === 'Enter') { 
      this.onSearch();
    }
  }

  handleToggleUser = (eid) => {
    const { usersToAdd, searchResults } = this.state;
    
    if (usersToAdd.some(u => u.eid === eid)) {
      this.setState({usersToAdd: usersToAdd.filter(u => u.eid !== eid)});
    } else {
      usersToAdd.push(searchResults.find(u => u.eid === eid));
      this.setState({usersToAdd: usersToAdd});
    }
  }

  render() {
    const { classes, handleClose, handleAdd, userType = 'participant', currentUsers } = this.props;
    const { searchTerm, searchResults, usersToAdd, isSearching } = this.state;

    return (
      <Dialog
        open={true}
        classes={{ paper: classes.dialogPaper }}
        maxWidth={false}
      >
        <Grid container direction='column' spacing={16} className={classes.dialogContent}>
          {/* Title and 'X' */}
          <Grid container item justify='space-between' className={classes.headerGrid}>
            <Grid item>
              <Typography className={classes.headerText} align='center'>
                Add {userType}s
              </Typography>
            </Grid>
            <Grid item>
              <IconButton onClick={handleClose} className={classes.iconButton}>
                <CloseOutlined className={classes.closeIcon} />
              </IconButton>
            </Grid>
          </Grid>

          {/* User Boxes */}
          <Grid container item spacing={8}>
            {usersToAdd.map(u => 
              <Grid item key={u.eid}>
                <Grid item className={classes.userBox}>
                  <Typography className={classes.userBoxName}>
                    <span>
                      {u.name} <IconButton onClick={() => this.handleToggleUser(u.eid)} className={classes.iconButton}>
                        <CloseOutlined className={classes.deleteUserIcon} />
                      </IconButton>
                    </span>
                  </Typography>
                </Grid>
              </Grid>
            )}
          </Grid>

          {/* Search Bar and Label */}
          <Grid container item direction='column'>
            <Grid item className={classes.searchLabelGrid}>
              <Typography className={classes.searchLabel}>
                Select Class {userType === 'participant' ? 'Participants' : 'Facilitators'}
              </Typography>
            </Grid>
            <Grid item className={classes.searchBarGrid}>
              <TextField
                placeholder={`Search ${userType === 'participant' ? 'Participants' : 'Facilitators'} by Name, Email or EID`}
                className={classes.searchBar}
                name='search'
                value={searchTerm}
                variant='outlined'
                onChange={this.onSearchChange}
                onKeyPress={this.onKeyPress}
              />
              <IconButton onClick={this.onSearch} className={classes.searchIconButton}>
                <SearchOutlined className={classes.searchIcon} />
              </IconButton>
            </Grid>
          </Grid>

          {/* Search Results */}
          
          {isSearching &&
            <Grid item>
              <Loading extraStyle={{color: 'white'}}/>
            </Grid>
          }
          {!isSearching && searchResults.length > 0 &&
            <Grid container item direction='column' spacing={16}>
              {searchResults.map(u => 
                <Grid item key={u.eid}>
                  <FormControlLabel 
                    label={
                      <Typography className={currentUsers.some(user => user.eid === u.eid) ? classes.disabledSearchResult : classes.searchResult}>
                        {u.eid} - {u.name}
                      </Typography>
                    }
                    control={
                      <Checkbox 
                        checked={usersToAdd.some(user => user.eid === u.eid) || currentUsers.some(user => user.eid === u.eid)}
                        disabled={currentUsers.some(user => user.eid === u.eid)}
                        onChange={() => this.handleToggleUser(u.eid)}
                        className={classes.searchResultCheckbox}
                      />
                    }
                  />
                </Grid>
              )}
            </Grid>
          }

          {/* Add Button */}
          {usersToAdd.length > 0 &&
            <Grid container item xs={12} justify='center'>
              <Grid item>
                <Button onClick={() => handleAdd(usersToAdd)} className={classes.button}>
                  Add
                </Button>
              </Grid>
            </Grid>
          }
        </Grid>
      </Dialog>
    );
  }

}

export default withStyles(styles)(NewUserModal);