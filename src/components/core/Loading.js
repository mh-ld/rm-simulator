//@flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

const Loading = ({ classes, size, text, extraStyle = {}, containerStyle = {} }) => {
  return (
    <div className={classes.container} style={containerStyle}>
      <CircularProgress
        className={classes.inProgress}
        size={size || 80}
        style={extraStyle}
        color='inherit'
      />
      { text && (
        <div style={{textAlign: 'center'}}>
          <Typography>{ text }</Typography>
        </div>
      )}
    </div>
  );
};

const styles = {
  container: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black'
  },
  inProgress: {
    margin: 20
  }
};

export default withStyles(styles)(Loading);