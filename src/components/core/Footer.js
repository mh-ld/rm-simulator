import React, { Component } from 'react';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Grid } from '@material-ui/core';

class Footer extends Component {
  goToLink = (link, event) => {
    event.stopPropagation();
    this.props.history.push(link);
  }

  render() {
    const { classes } = this.props;
    const year = moment().format('YYYY');

    return (
      <footer className={classes.footer}>
        <div className={classes.footerBottom}>
          <div className={classes.widthWrapper}>
            <Grid container alignItems="center" className={classes.bottomLine}>
              <Grid item>
                <img alt="logo" style={{marginTop: 10, width: 64, marginRight: 32 }} src='https://isistudio-public.s3.amazonaws.com/logo.png' />
              </Grid>
              <Grid item xs={12} sm={'auto'}>
                <Typography className={classes.bottomLineText}>© {year} Marriott International. All rights reserved.</Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.bottomLineText}><a rel="noopener noreferrer" className={classes.footerLink} href="http://www.marriott.com/marriott/privacy-us.mi" target="_blank">Privacy Policy</a></Typography>
              </Grid>
              <Grid>
                <Typography className={classes.bottomLineText}><a rel="noopener noreferrer" className={classes.footerLink} href="http://www.marriott.com/marriott/termsofuse-us.mi" target="_blank">Terms of Service</a></Typography>
              </Grid>
            </Grid>
          </div>
        </div>
      </footer>
    );
  }
};

const styles = theme => ({
  topFooterGrid: {
    height: '100%'
  },
  text: {
    color: '#f4f4f4',
    fontSize: 18
  },
  navText: {
    color: '#f4f4f4',
    fontWeight: 600,
    fontSize: 14,
    display: 'inline',
    paddingRight: 32,
    cursor: 'pointer',
    textTransform: 'uppercase',
    fontFamily: 'ProximaNova-Semibold',
    letterSpacing: '1px'
  },
  bottomLine: {
    marginTop: 16
  },
  bottomLineText: {
    color: '#f4f4f4',
    fontSize: 12,
    display: 'inline',
    paddingRight: 32
  },
  footer: {
    width: '100%',
    height: 200
  },
  widthWrapper: {
    maxWidth: 1248,
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      padding: '0 16px'
    },
    height: '100%'
  },
  footerTop: {
    height: 87,
    width: '100%',
    backgroundColor: '#000',
    borderBottom: '1px solid #777'
  },
  rightText: {
    [theme.breakpoints.up('sm')]: {
      marginLeft: 'auto',
      paddingLeft: 10,
      textAlign: 'right'
    }
  },
  footerBottom: {
    minHeight: 113,
    width: '100%',
    backgroundColor: '#000',
    [theme.breakpoints.up('sm')]: {
      padding: `24px 0 40px 0`
    },
    [theme.breakpoints.down('xs')]: {
      padding: `24px 0 40px 0`
    }
  },
  footerLink: {
    color: 'inherit',
    textDecoration: 'none'
  }
});

export default withRouter(withStyles(styles)(Footer));
