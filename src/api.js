import axios from 'axios';
import { apiURL } from './config';
import authStore from './stores/AuthStore';

const POST = 'POST';
const GET = 'get';
const PUT = 'put';
const DELETE = 'delete';

const initApi = () => {
  const api = axios.create();
  api.defaults.baseURL = apiURL;

  return api;
};

const axiosApi = initApi();

const redirectOn401 = (error) => {
  if (error.response.data) {
    if (error.response.data.ssoUrl) {
      window.location.href = error.response.data.ssoUrl + window.location.pathname + window.location.search;
    }
    else {
      window.location.href = '/login';
    }
  }
};

const execute = async (verb, url, body, headers) => {
  try {
    const config = {
      url,
      method: verb,
      data: body,
      withCredentials: true,
      headers
    };
    const { status, data } = await axiosApi.request(config);

    //  reset auth session expiry on successful api return
    authStore.resetExpiryDate();

    return {
      status,
      data
    };
  } catch (error) {
    const { status } = error.response;
    // handle 401s in the store, payload may contain relevant redirect info
    if (status === 401) {
      redirectOn401(error);
    }
    console.error(error);
  }
};

class CECSimApi {
  getUserProfile() {
    return execute(GET, '/users/me');
  }

  getUserClass() {
    return execute(GET, '/rm2user/class');
  }

  getUserRoles() {
    return execute(GET, '/user/roles');
  }

  getUserByEid(eid) {
    return execute(GET, `/rm2users/${eid}`);
  }

  keepAlive() {
    //return execute(GET, '/auth/keepAlive');
    return execute(GET, '/users/me');
  }

  getCases() {
    return execute(GET, '/rm2cases');
  }

  getStep(caseId, stepId) {
    return execute(GET, `/case/${caseId}/step/${stepId}`)
  }

  updateStartedCase(caseId) {
    return execute(POST, '/started-case', { caseId: caseId });
  }

  getClasses() {
    return execute(GET, '/classes');
  }

  getMyClasses(isAdmin = false) {
    return execute(GET, `/rm2classes/mine/${isAdmin}`);
  }

  createClass(name, startDate, facilitators, participants) {
    return execute(POST, '/rm2class', { name, startDate, facilitators, participants })
  }

  editClass(name, startDate, facilitators, participants, classId) {
    return execute(PUT, `/rm2class/${classId}`, { name, startDate, facilitators, participants })
  }

  closeClass(classId) {
    return execute(PUT, `/rm2class/close/${classId}`)
  }

  removeClass(classId) {
    return execute(DELETE, `/rm2class/${classId}`)
  }

  getClass(classId) {
    return execute(GET, `/rm2class/${classId}`);
  }

  getSubmissions() {
    return execute(GET, `/rm-submissions`);
  }

  getMySubmissions() {
    return execute(GET, `/submissions/mine`);
  }
  
  createStepData(caseId, stepId) {
    return execute(POST, `/step-data`, { caseId, stepId });
  }

  getStepData(stepDataId) {
    return execute(GET, `/step-data/${stepDataId}`);
  }

  getAllUserStepData(learnerEid) {
    return execute(GET, `/step-data/user/${learnerEid}`);
  }

  getLatestUserStepData(learnerEid) {
    return execute(GET, `/step-data/user/latest/${learnerEid}`);
  }

  getUserStepData(learnerEid, caseId, stepId) {
    return execute(GET, `/step-data/user/${learnerEid}/${caseId}/${stepId}`);
  }

  getLatestClassStepDatas(classId) {
    return execute(GET, `/step-data/class/latest/${classId}`);
  }

  checkUserCanEdit(learnerEid, caseId, stepId) {
    return execute(GET, `/step-enabled/${learnerEid}/${caseId}/${stepId}`);
  }

  createSubmission(caseId, stepId, data, popupHasShown, submitted) {
    return execute(POST, `/rm2submission/${caseId}/${stepId}`, { data, popupHasShown, submitted });
  }

  uploadRMRecording(recording) {
    return execute(POST, `/rm/recording`, recording, {'Content-Type': 'multipart/form-data'});
  }

  giveFeedback(stepDataId, response, approved) {
    return execute(POST, `/rm2feedback/${stepDataId}`, { response, approved });
  }

  searchUsers(searchTerm, limit) {
    return execute(GET, `/search/users/${searchTerm}?limit=${limit}`);
  }
}

export default new CECSimApi();