export default async function hasRecordingDevice() {
  const devices = await navigator.mediaDevices.enumerateDevices();
  
  return devices.some((device) => {
    if(device.kind === 'audioinput') {
      return true;
    } else {
      return false;
    }
  });
}