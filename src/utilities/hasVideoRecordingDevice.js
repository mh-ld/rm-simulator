export default async function hasVideoRecordingDevice() {
  const devices = await navigator.mediaDevices.enumerateDevices();
  
  return devices.some((device) => {
    if(device.kind === 'videoinput') {
      return true;
    } else {
      return false;
    }
  });
}